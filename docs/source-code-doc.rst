###############
SOURCE CODE DOC
###############

************
Entry Point
************

Package import
==============

multiRNAseq
---------------------------------

.. automodule:: multiRNAseq
   :members:

command line interface
---------------------------------

.. automodule:: multiRNAseq.command_line
   :members:

=============================================================================

*******
Errors
*******

Error/Warnings
==============

.. automodule:: multiRNAseq.errors
   :members:

=============================================================================

**************
Structure
**************

Common modifications
======================

Shared files
