#######
USAGE
#######

**********
SYNOPSIS
**********

.. argparse::
   :ref: multiRNAseq.command_line._cli
   :prog: multiRNAseq

**************
Instructions
**************

User configuration
====================

- Create a configuration file as directed `here <configure.html>`__.
