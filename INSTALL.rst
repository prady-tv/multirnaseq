***************
Prerequisites
***************

- Python3
- pip

********
Install
********

pip
====
Preferred method

Install
--------

.. code:: sh

    pip install multiRNAseq


Update
-------

.. code:: sh

    pip install -U multiRNAseq


Uninstall
----------

.. code:: sh

    pip uninstall -y multiRNAseq



`pspman <https://github.com/pradyumna/pspman>`__
=====================================================

(Linux only)

For automated management: updates, etc


Install
--------

.. code:: sh

   pspman -s -i https://github.com/pradyumna/multiRNAseq.git



Update
-------

.. code:: sh

    pspman


*That's it.*


Uninstall
----------

Remove installation:

.. code:: sh

    pspman -s -d multiRNAseq
