#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Biological knowledge codes
"""
from typing import Dict

# Nucleotide encodings     TACG    COMP
_X = 0x00  #               0000    0000
_G = 0x01  #               0001    0010
_C = 0x02  #               0010    0001
_S = _G | _C  #            0011    1100
_A = 0x04  #               0100    1000
_R = _A | _G  #            0101    1010
_M = _A | _C  #            0110    1001
_V = _A | _G | _C  #       0111    1011
_T = 0x08  #               1000    0100
_K = _G | _T  #            1001    0110
_Y = _C | _T  #            1010    0101
_B = _G | _C | _T  #       1011    0111
_W = _A | _T  #            1100    0011
_D = _A | _G | _T  #       1101    1110
_H = _A | _C | _T  #       1110    1101
_N = _A | _T | _G | _C  #  1111    1111
_MASK = 0x20
_GAP = 0x40
_ = 0x80  # For Future
_RNA = 0x10
_FLAG = 0xF0

CODE: Dict[str, int] = {
    '-': _X | _GAP,
    'G': _G,
    'C': _C,
    'S': _S,
    'A': _A,
    'R': _R,
    'M': _M,
    'V': _V,
    'T': _T,
    'U': _T | _RNA,
    'K': _K,
    'Y': _Y,
    'B': _B,
    'W': _W,
    'D': _D,
    'H': _H,
    'N': _N,
    'g': _G | _MASK,
    'c': _C | _MASK,
    's': _S | _MASK,
    'a': _A | _MASK,
    'r': _R | _MASK,
    'm': _M | _MASK,
    'v': _V | _MASK,
    't': _T | _MASK,
    'u': _T | _RNA | _MASK,
    'k': _K | _MASK,
    'y': _Y | _MASK,
    'b': _B | _MASK,
    'w': _W | _MASK,
    'd': _D | _MASK,
    'h': _H | _MASK,
    'n': _N | _MASK,
}

DECODE: Dict[int, str] = {
    _X: '-',
    _GAP: '-',
    _G: 'G',
    _C: 'C',
    _S: 'S',
    _A: 'A',
    _R: 'R',
    _M: 'M',
    _V: 'V',
    _T: 'T',
    _T | _RNA: 'U',
    _K: 'K',
    _Y: 'Y',
    _B: 'B',
    _W: 'W',
    _D: 'D',
    _H: 'H',
    _N: 'N',
    _G | _MASK: 'g',
    _C | _MASK: 'c',
    _S | _MASK: 's',
    _A | _MASK: 'a',
    _R | _MASK: 'r',
    _M | _MASK: 'm',
    _V | _MASK: 'v',
    _T | _MASK: 't',
    _T | _RNA | _MASK: 'u',
    _K | _MASK: 'k',
    _Y | _MASK: 'y',
    _B | _MASK: 'b',
    _W | _MASK: 'w',
    _D | _MASK: 'd',
    _H | _MASK: 'h',
    _N | _MASK: 'n',
}
