#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Command-line Callable
"""

import shutil
import sys
from pathlib import Path

from psprint import print

from . import CONFIG
from .command_line import cli
from .map_hisat import hisat_mappings
from .process_prep import prepare
from .read_config import read_config
from .shell import Shell


def main():
    design = cli()['design']

    # Load Configuration
    config = read_config(config_file=design, config=CONFIG)
    config.validate()
    print(config, mark="info")

    # Create environment
    shell = Shell(config.execs)

    # make directories
    for attr in config.resources.__dict__.values():
        if isinstance(attr, Path):
            attr.mkdir(exist_ok=True, parents=True)

    # Preapare reads
    if (err_list := prepare(config.genomes, config.phases, shell,
                            config.resources.max_threads)):
        print('Error during preparation : [download/indexing]', mark='err')
        print('aborting...', mark='err')
        print('Following handle(s) threw error(s):', file=sys.stdout)
        for err_str in err_list:
            print(err_str, mark='list', file=sys.stdout)
        return 1

    # map reads
    align_dict = hisat_mappings(config.phases, config.genomes,
                                config.vals.target, shell, config.resources)
    align_error = False
    for align_map, align_h in align_dict.items():
        if align_h is None:
            align_error = True
            print(f'Error mapping {align_map}', mark='list', file=sys.stdout)
    if align_error:
        print('aborting...', mark='err')
        return 1

    # Analyse mapping summary

    # Count TPMs

    try:
        shutil.rmtree(config.resources.tmp)
    except PermissionError:
        print(f'Scars {config.resources.tmp}, permission denied', mark='warn')
    print("Analysis completed.", mark='info')
    return 0


if __name__ == '__main__':
    main()
