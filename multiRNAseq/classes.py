#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Data handle classes
"""
import os
from pathlib import Path
from shutil import which
from typing import Dict, List, Optional

from multiRNAseq.pathmod import fastq_base_split

from .biomol import DNA
from .errors import DependencyError, DesignFileValueError, TargetError


class Resources():
    """
    Default compute resoruces

    Attributes:
        max_threads: maximum number of threads to use
        root: root dir for analysis
        tmp: directory to store temporary files
        genome: default location of genome file
        rnaseq: default location of RNASeq files
        ncbi: ncbi root directory (used to delete some sra data and make space)

    Args:
        design: design configuration object

    """
    def __init__(self, design: Dict[str, str] = None):
        self.max_threads = 1
        self._root = None
        self._tmp = None
        self._genome = None
        self._rnaseq = None
        self._ncbi = None
        if design:
            self.update(design)

    def __repr__(self) -> str:
        return f"""
    analysis root: {self.root}
    self.tmp: {self.tmp}
    genome dir: {self.genome}
    rnaseq dir: {self.rnaseq}
    ncbi dir: {self.ncbi}
"""

    @property
    def root(self) -> Path:
        if self._root is not None:
            return self._root
        return Path(os.getcwd())

    @root.setter
    def root(self, value):
        if value is None:
            self._root = None
            return
        self._root = Path(value)

    @root.deleter
    def root(self):
        self._root = None

    @property
    def tmp(self) -> Path:
        if self._tmp is not None:
            return self._tmp
        return self.root / "tmps"

    @tmp.setter
    def tmp(self, value):
        if value is None:
            self._tmp = None
            return
        self._tmp = Path(value)

    @tmp.deleter
    def tmp(self):
        self._tmp = None

    @property
    def genome(self) -> Path:
        if self._genome is not None:
            return self._genome
        return self.root / "genomes"

    @genome.setter
    def genome(self, value):
        if value is None:
            self._genome = None
            return
        self._genome = Path(value)

    @genome.deleter
    def genome(self):
        self._genome = None

    @property
    def rnaseq(self) -> Path:
        if self._rnaseq is not None:
            return self._rnaseq
        return self.root / "rnaseq"

    @rnaseq.setter
    def rnaseq(self, value):
        if value is None:
            self._rnaseq = None
            return
        self._rnaseq = Path(value)

    @rnaseq.deleter
    def rnaseq(self):
        self._rnaseq = None

    @property
    def ncbi(self) -> Path:
        if self._ncbi is not None:
            return self._ncbi
        return self.root / "ncbi"

    @ncbi.setter
    def ncbi(self, value):
        if value is None:
            self._ncbi = None
            return
        self._ncbi = Path(value)

    @ncbi.deleter
    def ncbi(self):
        self._ncbi = None

    def update(self, design: Dict[str, str]):
        """
        Update attributes

        Args:
            design: design configuration object

        """
        max_threads = design.get('max threads')
        if not max_threads:
            max_threads = len(os.sched_getaffinity(0))
        self.max_threads = int(max_threads)
        for key, value in design.items():
            if hasattr(self, key):
                setattr(self, key, value)


class Executables():
    """
    Reference for path to executables
    ** Call ``verify`` method before using **

    Attributes:
        execute: bool: whether actions are to be executed
        aria2c: path to aria2c (optional)
        prefetch: path to NCBI prefetch
        fastq-dump: path to NCBI fastq-dump
        hisat2: path to HISAT2 executable
        hisat2_build: path to HISAT2-build indexer
        samtools: path to samtools
        TPMCalculator: path to NCBI TPMCalculator

    Args:
        design: design configuration object

    """
    def __init__(self, design: Dict[str, str] = None):
        self._verified = False
        self.execute = False
        self.dependencies: Dict[str, Optional[Path]] = {
            'gzip': None,
            'aria2c': None,
            'prefetch': None,
            'fastq-dump': None,
            'fastqc': None,
            'trim_galore': None,
            'cutadapt': None,
            'hisat2-build': None,
            'hisat2': None,
            'samtools': None,
            'TPMCalculator': None,
        }
        if design is not None:
            self.update(design)
        for dep in self.dependencies:
            path_to_dep = which(dep)
            if path_to_dep is not None:
                self.dependencies[dep] = Path(path_to_dep)

    def __getitem__(self, key: str):
        """
        self[key] -> self.dependencies[key]
        """
        if self._verified:
            return self.dependencies[key]
        else:
            raise DependencyError("verification")

    def __iter__(self):
        """
        Iterate over known dependencies
        """
        return self.dependencies.__iter__()

    def __repr__(self) -> str:
        return f'''
    execute?: {self.execute}
    {self.dependencies.__repr__()}
'''

    def update(self, design: Dict[str, str]):
        """
        Update attributes

        Args:
            design: design configuration object

        """
        for dep in self.dependencies:
            if dep in design and design[dep] is not None:
                self.dependencies[dep] = Path(design[dep])
        if design.get('execute'):
            self.execute = True

    def verify(self):
        """
        Verify that all dependencies are available
        """
        for dep, loc in self.dependencies.items():
            if loc is None or not loc.is_file():
                raise DependencyError(dep)
        self._verified = True


class Genome():
    """
    Genome handle

    Attributes:
        name: Canonical scientific name of organism
        sysname: machine_friendly name (' ' -> '_')
        fasta: fasta sequence filepath
        gff: annotation filepath
        gff: transcript-annotation filepath
        hisat_index: hisat index file base
        hisat_exons: hisat exons table path
        hisat_splicesites: hisat exons table path
        hisat_flags: flags passed to hisat while mapping to this genome

    Args:
        name: name of organism [format: genus<space>species<strai>...]
        **kwargs:
            update attributes
            rest are ignored
    """
    def __init__(self, name: str, genome_root: Path, **kwargs):
        self.name = name
        self.sysname = name.replace(' ', '_')
        self._raw_paths = {
            'fasta': kwargs.get('fasta'),
            'gff': kwargs.get('gff'),
            'gtf': kwargs.get('gtf'),
            'hisat2_index': kwargs.get('hisat2_index'),
            'hisat2_exons': kwargs.get('hisat2_exons'),
            'hisat2_splicesites': kwargs.get('hisat2_splicesites'),
        }
        self.root = genome_root / name.replace(" ", "/")
        self.hisat2_index = self.root / name.replace(' ', '_')
        self.fasta = self.root / (name.replace(' ', '_') + "_sequence.fna")
        self.gff = self.root / (name.replace(' ', '_') + "_annotation.gff")
        self.gtf = self.root / (name.replace(' ', '_') + "_annotation.gtf")
        self.hisat2_exons = self.root / (name.replace(' ', '_') + "_exons.txt")
        self.hisat2_splicesites = self.root / (name.replace(' ', '_') +
                                               "_splicesites.txt")
        self.hisat_flags: List[str] = kwargs.get('hisat_flags', [])

    def __repr__(self) -> str:
        return f"""
    name: {self.name}
    directory root: {self.root}
    Sequence file: {self.fasta}
    source: {self._raw_paths['fasta']}
    annotation file: {self.gff}
    source: {self._raw_paths['gff']}
    annotation file: {self.gtf}
    source: {self._raw_paths['gtf']}
    hisat2_index: {self.hisat2_index}
    source: {self._raw_paths['hisat2_index']}
    hisat2_exons: {self.hisat2_exons}
    source: {self._raw_paths['hisat2_exons']}
    hisat2_splicesites: {self.hisat2_splicesites}
    source: {self._raw_paths['hisat2_splicesites']}
    hisat_flags: {self.hisat_flags}
"""


class TargetConfig():
    """
    Configuration

    Attributes:
        target: target genome
        info_len: minimum length of nucleotides for non-random information
        leader: leader sequence
        term: termination sequence
        min_qual: minimum phred quality that is `good`
        mismatch: fraction of match allowed as mismatch
    Args:
        design: design configuration object

    """
    def __init__(self, design: Dict[str, str] = None):
        self.target: str
        self.info_len = 20
        self.leader = DNA('')
        self.term = DNA('')
        self.min_qual: int = 30
        self.mismatch: float = 0.1
        if design is not None:
            self.update(design)

    def __repr__(self) -> str:
        return f"""
    target: {self.target}
    informative length: {self.info_len}
    leader: {self.leader}
    term: {self.term}
    min_qual: {self.min_qual}
    mismatch: {self.mismatch}
"""

    def update(self, design: Dict[str, str]):
        """
        Update attributes

        Args:
            design: design configuration object
        """
        if 'target' in design:
            self.target = design['target']
        if 'informative length' in design:
            self.info_len = int(design['informative length'])
        if 'leader' in design:
            self.leader = DNA(design['leader'])
        if 'term' in design:
            self.term = DNA(design['term'])
        if 'min_qual' in design:
            self.min_qual = int(design['min_qual'])
        if 'mismatch' in design:
            self.mismatch = float(design['mismatch'])

    def verify(self, known_genomes: Dict[str, Genome]):
        """
        Verify operability before use

        Raises:
            TargetError: Target not listed in genomes
            DesignFileValueError: bad value for mismatch
        """
        if not hasattr(self, 'target') or self.target is None:
            raise TargetError()
        if self.target not in known_genomes:
            raise TargetError(self.target)
        if not 0 <= self.mismatch <= 1:
            raise DesignFileValueError('mismatch', self.mismatch)
        if self.info_len < 0:
            raise DesignFileValueError('informative length', self.info_len)


class ExptRun():
    """
    Attributes:
        accession: accession of experiment
        contaminants: contaminating genomes
        forward: forward reads (or single reads)
        reverse: reverse reads (or ``None``)
        base: name base for replicate
        seqtype: sequencing method: mirnaseq, slsrq, rnaseq
        hisat_flags: flags passed to hisat while mapping to this genome

    Args:
        accession: accession number
        parent_dir: downloaded files are in this directory
        contaminants: contaminant genome names
        seqtype: sequencing method
    """
    def __init__(self,
                 accession: str,
                 parent_dir: Path = None,
                 contaminants: List[str] = None,
                 seqtype: str = 'rnaseq',
                 hisat_flags: List[str] = None):
        self.accession = accession
        self.contaminants = contaminants
        self.parent_dir = parent_dir
        self.seqtype = seqtype.lower()
        self.hisat_flags = hisat_flags or []
        self.forward, self.reverse, self.base = None, None, None
        for punc in (' ', '\t', '_', '-', '/', '"', "'"):
            self.seqtype = self.seqtype.replace(punc, '')
        self.which_reads()

    def __repr__(self) -> str:
        return f"""
    {self.accession}:
    contaminants: {self.contaminants}
    phase dir: {self.parent_dir}
    paired: {bool(self.reverse)}
    sequencing type-seq: {self.seqtype}
    hisat_flags: {self.hisat_flags}

"""

    def which_reads(self):
        """
        Which reads are dumped for the experiments

        Returns:
            Paths to [forward] reads, [reverse reads or ``None``]
        """
        self.forward, self.reverse = Path(''), Path('')
        for ext in ('_1.fastq.gz', '.fastq.gz', '_1.fastq', '.fastq', 'miss_'):
            self.forward = self.parent_dir / (self.accession + ext)
            if self.forward.is_file():
                break
        if 'miss_' in self.forward.name:
            self.forward = None
        for ext in ('_2.fastq.gz', '_2.fastq', 'miss_'):
            self.reverse = self.parent_dir / (self.accession + ext)
            if self.reverse.is_file():
                break
        if 'miss_' in self.reverse.name:
            self.reverse = None
        if self.forward is not None:
            self.base = self.forward.parent / fastq_base_split(self.forward)[0]


class Phase():
    """
    Phase: a defined biological condition for RNAseq.
    Assumed to be in steady-state.

    Attributes:
        name; phase name
        root: parent directory for all experiments
        replicates: named experiments

    Args:
        replicates: design configuration object
    """
    def __init__(self, name: str, read_root: Path,
                 replicates: List[Dict[str, str]]):
        self.name = name
        self.root = read_root / self.name.replace(" ", "_")
        self.replicates: Dict[str, ExptRun] = {}
        for expt in replicates:
            self.replicates[expt['accession']] = ExptRun(parent_dir=self.root,
                                                         **expt)

    def __repr__(self) -> str:
        replicates = "\n    ".join(repr(r) for r in self.replicates.values())

        return f"""
    name: {self.name}
    directory_root: {self.root}
    replicates:
    {replicates}
"""
