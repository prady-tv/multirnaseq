#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Pathname modifications
"""

from os import PathLike
from pathlib import Path
from shutil import copy
from tempfile import NamedTemporaryFile
from typing import List, Optional, Tuple


def fastq_mod(*adds: str, fpath: PathLike, gz: bool = None, fq=None) -> Path:
    """
    Modify path of fastq file-name to add a qualifier in the name
    - Expected fastq regexp: ^.*(?:_[12])?.fastq(?:.gz)?$

    Args:
        *adds: qualifications to be added to path name
            - if "%" is in adds, _1/_2 will be dropped assuming HiSAT2
        fpath: path to modify (filename)
        gz: whether to add .gz?

            - ``True``: Force-add
            - ``False``: Force-remove
            - ``None``: Do nothing

        fq: Shorten `.fastq` suffix to `.fq`

            - ``True``: Force-shorted
            - ``False``: Force-lengthen
            - ``None``: Do nothing


    Returns:
        Modified path

    """
    fpath = Path(fpath)
    name, fname_ext = fastq_base_split(fpath)

    # HiSAT mod
    if "%" in adds:
        # "%" implies _1 and _2 will be provided by hisat
        fname_ext = list((ori for ori in fname_ext if ori not in ("_1", "_2")))

    # shorten fastq?
    # This block is a result of lack of standards in naming fastq files
    if fq:
        if '.fastq' in fname_ext:
            fname_ext[fname_ext.index('.fastq')] = '.fq'
    elif fq is False:
        if '.fq' in fname_ext:
            fname_ext[fname_ext.index('.fq')] = '.fastq'

    # Add gz?
    if gz:
        if fname_ext[0] != ".gz":
            fname_ext.insert(0, ".gz")
    elif gz is False:
        if '.gz' in fname_ext:
            fname_ext.remove('.gz')

    return fpath.parent / ('_'.join((name, *adds)) + "".join(fname_ext[::-1]))


def fastq_base_split(fpath: PathLike, /) -> Tuple[str, List[str]]:
    """
    Convert fastq file path to extract base
    base excludes extensions list: _1, _2, .fastq, .gz

    Args:
        fpath: path of fastq reads file

    Returns Base of fastq reads and extensions
    """
    fpath = Path(fpath)  # DONT resolve this path
    fq_ext = (".gz", '.fq', ".fastq", "_2", "_1")  # preserve this order
    fname_ext: List[str] = []
    name = fpath.name
    for ext in fq_ext:
        len_ext = len(ext)
        if name[-len_ext:] == ext:
            name = name[:-len_ext]
            fname_ext.append(ext)
    return name, fname_ext


def cat_files(up_path: PathLike = None,
              down_path: PathLike = None,
              /,
              combined_path: Optional[PathLike] = None) -> Optional[Path]:
    """
    Catenate two files passed by path

    Args:
        up_path: path to file whose contents appear first
        down_path: path to file whose contents appear last
        combined_path: path to combined file

            - May be same as any of `up_path` or `down_path` **BE CAUTIOUS**
            - if none, `up_path`, `down_path` names are appended

    """
    if up_path is None:
        if down_path is None:
            return None
        if combined_path is None:
            return Path(down_path)
        copy(down_path, combined_path)
        return Path(combined_path)
    up_path = Path(up_path)
    if down_path is None:
        if combined_path is None:
            return up_path
        copy(up_path, combined_path)
        return Path(combined_path)
    down_path = Path(down_path)
    if combined_path is None:
        combined_path = up_path.parent / (up_path.stem + '_' + down_path.stem +
                                          up_path.suffix)
    with NamedTemporaryFile() as cat_h:
        cat_h.write(up_path.read_bytes())
        cat_h.write(down_path.read_bytes())
        cat_h.flush()
        copy(cat_h.name, Path(combined_path))
    return Path(combined_path)
