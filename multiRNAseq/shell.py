#!/usr/bin/env python3
# -*- coding:utf-8; mode:python -*-
#
# Copyright 2020-2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
common shell calls functions
"""

import os
import subprocess
import sys
from typing import Optional

from .classes import Executables
from .errors import ExternalError


class Shell():
    """
    Custom shell calls opened by popen

    Attributes:
        exec_env: Executables: known executable program paths (rest are called
        directly)

    Args:
        exec_env: Executables: Initialized `executables`

    """
    def __init__(self, exec_env: Executables = None):
        self.exec_env = exec_env or Executables()

    def process_comm(self,
                     *cmd,
                     p_name: str = 'processing',
                     timeout: int = None,
                     fail: str = 'silent',
                     **kwargs) -> Optional[str]:
        """
        Generic process definition and communication

        Args:
            *cmd: list(args) passed to subprocess.Popen as first arguments
            p_name: notified as 'Error {p_name}: {stderr}
            timeout: communicatoin timeout. If -1, 'communicate' isn't called
            fail: on fail, perform the following:
                - notify: send stderr, return ``None``
                - raise: raise ExternalError
                - silent: return ``None``

            **kwargs: passed on to subprocess.Popen

        Returns:
            ``stdout`` from command if process exits without error
            ``None`` if process exits with error and fail is not raise

        Raises:
            ExternalError: if `fail` == "raise"

        """
        if os.environ.get("READTHEDOCS"):  # pragma: no cover
            # RTD virutal environment
            return None
        cmd_l: list = list(cmd)
        cmd_l = list(str(arg) for arg in cmd_l)
        if cmd_l[0] in self.exec_env:
            cmd_l[0] = str(self.exec_env[cmd_l[0]])
        print("running: ", *cmd_l)
        if not self.exec_env.execute:
            return "DRY RUN"
        if timeout is not None and timeout < 0:
            process = subprocess.Popen(cmd_l, **kwargs)  # DONT: *cmd_l here
            return None
        process = subprocess.Popen(cmd_l,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   text=True,
                                   **kwargs)
        stdout, stderr = process.communicate(timeout=timeout)
        if not process.returncode:
            return stdout
        if fail == 'raise':
            raise ExternalError(*cmd,
                                retcode=process.returncode,
                                stderr=stderr,
                                stdout=stdout)
        if fail == "notify":
            print(f'Error: {p_name}: Error Message: {stderr}', file=sys.stderr)
        return None
