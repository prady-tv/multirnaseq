#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
jitted functions
"""

from math import ceil
from typing import Tuple

import numba as nb
import numpy as np

from .biocode import _FLAG, _N


@nb.vectorize([nb.int64(nb.int64)])
def comp(nuc_bit: int) -> int:
    """
    complement of nucbit
    """
    flag = nuc_bit & _FLAG
    cun_bit = 0
    for sub_bit, comp_bit in enumerate([1, 0, 3, 2]):  # complement information
        cun_bit |= int(bool((nuc_bit & (1 << sub_bit)))) << comp_bit
    # hard set across bitgroups
    if cun_bit == 0x03:
        cun_bit = 0x0C
    elif cun_bit == 0x0C:
        cun_bit = 0x03
    return cun_bit | flag


def revcomp(nuc_seq: np.ndarray) -> np.ndarray:
    """
    Calculate the reverse complemented sequence

    Args:
        nuc_seq: nucleotide sequence

    Returns
        reverse complement of ``nuc_seq``
    """
    return np.flip(comp(nuc_seq)).astype(np.uint8)


@nb.jit(nopython=True, error_model="numpy", nogil=True)
def find_edge(read: np.ndarray,
              lead: np.ndarray = np.array([], dtype=np.uint8),
              rev_lead: np.ndarray = np.array([], dtype=np.uint8),
              term: np.ndarray = np.array([], dtype=np.uint8),
              rev_term: np.ndarray = np.array([], dtype=np.uint8),
              inform: int = 20,
              err_rate: float = 0.01) -> Tuple[int, int, int]:

    # find Leader
    stt = find_lead(read=read, lead=lead, inform=inform, err_rate=err_rate)
    if stt != 0:
        # leader found (such read ought not contain term)
        return 1, stt, read.size
    end = find_term(read=read, term=rev_lead, inform=inform, err_rate=err_rate)
    if end != read.size:
        # rev_leader found (such read ought not contain term)
        return 1, 0, end
    # find terminal
    end = find_term(read=read, term=term, inform=inform, err_rate=err_rate)
    if end != read.size:
        # term found
        return -1, 0, end
    stt = find_lead(read=read, lead=rev_term, inform=inform, err_rate=err_rate)
    if stt != 0:
        # rev_term found
        return -1, stt, read.size
    return 0, 0, read.size


def find_lead(read: np.ndarray,
              lead: np.ndarray,
              inform: int = 14,
              err_rate: float = 0.01) -> int:
    """
    Locate leader or termination sequence, if present, in the read
    - At least `inform` bases (I) and, if present,
    all leading bases (V) must match with allowed error_rate.

    Args:
        read: array of read sequence nucbits
        lead: array of leader or reverse complement of terminator nucbits
        err_rate: fraction of allowed errors
        inform: minimum number of bases that make an informative sequence

            - a sequence longer than
              ``1 + ceil(log(genome length)/log(4))``
              is expected to be found by mere serendipity,
              less than once in the genome. *Assuming no error nucleotides*

    Returns:
        start: anchor start (if edge, else 0)

    Legend::

        Lead: L <- Revcomp -> T : Term
        E: Edge of read
        R: Read nucleotides
        I: minimum Informative part of read
        V: nucleotides for Validation
        a: Found Anchor for information
        r: anchor for reverse comlement

        lead:          LLLLLLLLLLLLLLLLLLLLLLLLLLLL
        edge:                                      EEEEEEEEEEEEEEEEEEEEEEEE
        read:              RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
        info_lead:                    IIIIIIIIIIIII
        verify_lead:       VVVVVVVVVVV
        anchor:                       a
        return:                                    s

    """
    if lead.size == 0:
        # Leader sequence is not defined
        return 0
    min_pass = ceil(inform * (1 - err_rate))
    subject_read = read & _N
    info_lead = lead[-inform:] & _N
    matches = f_np_match(subject_read, info_lead, min_pass=min_pass)
    if matches.size == 0:
        # no matches found. Read doesn't contain leader or term
        return 0
    for anchor in np.flip(matches):
        # start with the greediest match
        verify_sub = subject_read[max(0, anchor+inform-lead.size):anchor]
        verify_lead = lead[-inform - anchor:-inform]

        if (np.bitwise_and(verify_lead, verify_sub) == verify_lead).astype(
                np.int64).sum() > int(verify_lead.size * (1 - err_rate)):
            # trim everything other than edge
            return anchor + inform
    return 0


def find_term(read: np.ndarray,
              term: np.ndarray,
              inform: int = 14,
              err_rate: float = 0.01) -> int:
    """
    Locate leader or termination sequence, if present, in the read
    - At least `inform` bases (I) and, if present,
    all leading bases (V) must match with allowed error_rate.

    Args:
        read: array of read sequence nucbits
        term: array of terminator or reverse complement of leader nucbits
        err_rate: fraction of allowed errors
        inform: minimum number of bases that make an informative sequence

            - a sequence longer than
              ``1 + ceil(log(genome length)/log(4))``
              is expected to be found by mere serendipity,
              less than once in the genome. *Assuming no error nucleotides*

    Returns:
        end: anchor end (if edge, else len(read))

    Legend::

        Lead: L <- Revcomp -> T : Term
        E: Edge of read
        R: Read nucleotides
        I: minimum Informative part of read
        V: nucleotides for Validation
        a: Found Anchor for information
        r: anchor for reverse comlement

        term:                                      TTTTTTTTTTTTTTTTTTTTTTTTTTTT
        edge:              EEEEEEEEEEEEEEEEEEEEEEEE
        read:              RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
        info_lead:                                 IIIIIIIIIIIII
        verify_lead:                                            VVVVVVVVVVV
        anchor:                                    a
        return:                                    e

    """
    if term.size == 0:
        # Termer sequence is not defined
        return read.size
    min_pass = ceil(inform * (1 - err_rate))
    subject_read = read & _N
    info_term = term[:inform] & _N
    matches = f_np_match(subject_read, info_term, min_pass=min_pass)
    if matches.size == 0:
        # no matches found. Read doesn't contain termer
        return read.size
    for anchor in matches:
        # start with the greediest match
        verify_sub = subject_read[anchor + inform:min(anchor+term.size,
                                                      read.size)]
        verify_term = term[inform:inform + verify_sub.size]

        if (np.bitwise_and(verify_term, verify_sub) == verify_term).astype(
                np.int64).sum() > int(verify_term.size * (1 - err_rate)):
            return anchor
    return read.size


@nb.jit(nopython=True, error_model="numpy", nogil=True)
def _rolling_window(subject: np.ndarray, size: int) -> np.ndarray:
    shape = subject.shape[-1] - size + 1, size
    strides = subject.strides + subject.strides
    return np.lib.stride_tricks.as_strided(subject,
                                           shape=shape,
                                           strides=strides)


def f_np_match(subject: np.ndarray, pattern: np.ndarray,
               min_pass: int) -> np.ndarray:
    """
    fuzzy-matching between subject and pattern

    Args:
        subject: search subject (target)
        pattern: search pattern
        min_pass: minimum number of nucleotides that must match
    """
    matches, = np.where((np.bitwise_and(_rolling_window(
        subject, pattern.size), pattern) == pattern).sum(axis=1) >= min_pass)
    return matches


nb.jit_module(
    nopython=True,
    error_model="numpy",
    nogil=True,
    cache=True,
    parallel=True,
)


def trim_bad(qual: np.ndarray, min_qual: int = 30) -> Tuple[int, int]:
    """
    Trim bad patches of the read.
    - If an internal bad patch is found, use longest contiguous good patch.

    Args:
        min_qual: minimum required quality of good base.

            - 0  -> 1 (100%) error
            - 10 -> 0.1 (10%) error
            - 20 -> 0.01 (1%) error
            - 30 -> 0.001 (0.1%) error
            - 40 -> 0.0001 (0.01%) error
    """
    # Find the indicies of changes with good quality
    good_qual = qual > min_qual
    edges: np.ndarray = np.diff(good_qual)
    idx, = edges.nonzero()

    # We need to start things after the change in "good_qual". Therefore,
    # we'll shift the index by 1 to the right.
    idx += 1

    if good_qual[0]:
        # If the start of good_qual is True prepend a 0
        idx = np.insert(idx, 0, 0).astype(np.uint8)

    if good_qual[-1]:
        # If the end of good_qual is True, append the length of the array
        idx = np.append(idx, good_qual.size).astype(np.uint8)

    idx.shape = (-1, 2)
    if idx is None or idx.size == 0:
        return 0, 0
    else:
        return tuple(sorted(idx, key=lambda x: x[1] - x[0]))[-1]

