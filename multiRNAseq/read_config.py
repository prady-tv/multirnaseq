#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Read default/custom design configuration files
"""

import os
from pathlib import Path
from typing import Dict

import yaml

from .classes import Executables, Genome, Phase, Resources, TargetConfig
from .errors import DesignFileFormatError, DesignFileNotFoundError


class Design():
    """
    Experimental Design reference object

    Attributes:
        resources: Material resources
        exe: Executable paths
        config: configuration
        genomes: reference genomes
        rnaseq: rna seq read files

    Args:
        design: yaml object read from file
    """
    def __init__(self, design: dict):
        self.resources = Resources()
        self.execs = Executables()
        self.vals = TargetConfig()
        self.genomes: Dict[str, Genome] = {}
        self.phases: Dict[str, Phase] = {}
        self.update(design)

    def __repr__(self):
        genomes = []
        rnaseq = []
        for genome, genome_info in self.genomes.items():
            genomes.append(genome)
            genomes.append(repr(genome_info))

        for phases, phase_info in self.phases.items():
            rnaseq.append(phases)
            rnaseq.append(repr(phase_info))

        genomes_repr = '\n    '.join(genomes)
        rnaseq_repr = '\n    '.join(rnaseq)

        return f"""
Resources:
{self.resources}

Executables:
    {self.execs}

Info:
{self.vals}

genomes:
    {genomes_repr}

Reads:
    {rnaseq_repr}

"""

    def update(self, design: dict = None):
        """
        Update experimental design

        Args:
            design: design configuration object

        """
        try:
            if design is None:
                raise KeyError
            self.resources.update(design["resources"])
            self.execs.update(design["executables"])
            self.vals.update(design["config"])
            for name, kwargs in design['genomes'].items():
                self.genomes[name] = Genome(name=name,
                                            genome_root=self.resources.genome,
                                            **kwargs)
            for phase, replicates in design['RNAseq'].items():
                self.phases[phase] = Phase(name=phase,
                                           read_root=self.resources.rnaseq,
                                           replicates=replicates)
        except KeyError as err:
            raise DesignFileFormatError(err)

    def validate(self):
        """
        Validate usability of instructions from design file
        """
        self.vals.verify(self.genomes)
        self.execs.verify()


def read_config(config_file: os.PathLike = None,
                config: Design = None) -> Design:
    """
    Read configuration file to populate

    Args:
        config_file: configuration of experimental design
        config: design configuration to update
    Returns:
        Experimental design configuration

    """
    if config_file is None:
        config_file = Path(__file__).parent / "default.yml"
    config_path = Path(config_file)
    if not config_path.is_file():
        raise DesignFileNotFoundError("Experimental design file not found")
    with open(config_path, 'r') as config_h:
        design = yaml.safe_load(config_h)
    if config is None:
        config = Design(design)
    else:
        config.update(design)
    return config
