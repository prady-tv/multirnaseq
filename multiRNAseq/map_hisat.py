#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Map reads to genome using HiSAT2
"""

import os
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List, Optional, Tuple, Union

from psprint import print

from .bam_handle import sam2bam, sort_bam
from .classes import ExptRun, Genome, Phase, Resources
from .errors import ExptError, TargetError
from .pathmod import fastq_base_split, fastq_mod
from .shell import Shell

SHELL = Shell()
CORES = len(os.sched_getaffinity(0))
RESOURCES = Resources()


@dataclass
class ReadMapOut():
    map_success: bool = False
    forward_mapped: Optional[Path] = None
    reverse_mapped: Optional[Path] = None
    forward_unmapped: Optional[Path] = None
    reverse_unmapped: Optional[Path] = None


def _hisat_xe(reads: List[Union[str, os.PathLike]],
              target: Genome,
              samfile: str,
              shell: Shell = SHELL,
              resources: Resources = RESOURCES,
              **kwargs) -> int:
    """
    Run hisat in single/paired end mode

    Args:
        reads: list: [-1, forward, -2, reverse] | [-U, forward]
        target: target genome
        shell: exec env
        resources: compute resources
        **kwargs:
            - opts: list[hisat options, order conserved]

    Returns:
        Failure

    Raises:
        RecursionError: summaries > 100
    """
    # define summary file name
    read_base = fastq_base_split(Path(reads[1]))[0]
    summary = Path(reads[1]).parent / '_'.join(
        (read_base, target.sysname, 'summary.txt'))
    recursion = 0
    while summary.exists():
        if recursion > 100:
            raise RecursionError('Too many summaries')
        summary = summary.parent / (summary.stem + f'_{recursion}.txt')
        recursion += 1

    opts = ('-p', min(resources.max_threads, CORES), '--summary-file', summary,
            '--new-summary', *kwargs.get('opts', []), *target.hisat_flags)
    cmd = ('hisat2', *opts, '-x', target.hisat2_index, *reads, '-S', samfile)
    hisat_out = shell.process_comm(*cmd, p_name=f'hisat_map', fail='notify')
    if hisat_out is None:
        return 1
    return 0


def hisat_se(expt: Tuple[Optional[Path], Optional[Path]],
             target: Genome,
             samfile: str,
             shell: Shell = SHELL,
             resources: Resources = RESOURCES,
             **kwargs) -> ReadMapOut:
    """
    Run hisat in single end mode

    Args:
        expt: expt to map
        target: target genome
        shell: exec env
        resources: compute resources
        **kwargs:
            - opts: list[hisat options, order conserved]

    Returns:
        Failure
    """
    forward, _ = expt
    if forward is None:
        # this file does not have forward reads file
        raise ExptError('Forward read not supplied')
    aligned = fastq_mod('aligned', target.sysname, fpath=forward, gz=True)
    unaligned = fastq_mod('unaligned', target.sysname, fpath=forward, gz=True)
    if (aligned.is_file() and unaligned.is_file()
            and Path(samfile.replace('.sam', '.bam')).exists()):
        print('Found already-existing mappings, using them', mark='warn')
        return ReadMapOut(map_success=True,
                          forward_mapped=aligned,
                          forward_unmapped=unaligned)
    opts = ('--un-gz', unaligned, '--al-gz', aligned, *kwargs.get('opts', []))
    map_error = _hisat_xe(reads=['-U', forward],
                          target=target,
                          shell=shell,
                          resources=resources,
                          samfile=samfile,
                          opts=opts)
    if map_error:
        return ReadMapOut(map_success=False)
    return ReadMapOut(map_success=True,
                      forward_mapped=aligned,
                      forward_unmapped=unaligned)


def hisat_map(expt: Tuple[Optional[Path], Optional[Path]],
              target: Genome,
              samfile: str,
              shell: Shell = SHELL,
              resources: Resources = RESOURCES,
              **kwargs) -> ReadMapOut:
    """
    Guess paired/single end mode and run hisat

    Args:
        expt: experiment to map or (read_path, read_path | ``None``)
        target: target genome
        shell: exec env
        resources: compute resources
        **kwargs:
            - opts: list[hisat options, order conserved]

    Returns:
        Failure
    """
    forward, reverse = expt
    if reverse is None:
        return hisat_se(expt,
                        target,
                        samfile=samfile,
                        shell=shell,
                        resources=resources,
                        **kwargs)
    if forward is None:
        # this file does not have forward reads file
        return ReadMapOut(map_success=False)
    aligned_base = fastq_mod('aligned',
                             target.sysname,
                             '%',
                             fpath=forward,
                             gz=True)
    unaligned_base = fastq_mod('unaligned',
                               target.sysname,
                               '%',
                               fpath=forward,
                               gz=True)
    forward_mapped = Path(str(aligned_base).replace("%", "1"))
    reverse_mapped = Path(str(aligned_base).replace('%', '2'))
    forward_unmapped = Path(str(unaligned_base).replace("%", "1"))
    reverse_unmapped = Path(str(unaligned_base).replace('%', '2'))
    if all(outfile.exists()
           for outfile in (forward_mapped, reverse_mapped, forward_unmapped,
                           reverse_unmapped,
                           Path(samfile.replace('.sam', '.bam')))):
        print('Found already-existing mappings, using them', mark='warn')
        return ReadMapOut(map_success=True,
                          forward_mapped=forward_mapped,
                          reverse_mapped=reverse_mapped,
                          forward_unmapped=forward_unmapped,
                          reverse_unmapped=reverse_unmapped)
    opts = ('--un-conc-gz', unaligned_base, '--al-conc-gz', aligned_base,
            *kwargs.get('opts', []))
    map_error = _hisat_xe(reads=['-1', forward, '-2', reverse],
                          target=target,
                          samfile=samfile,
                          shell=shell,
                          resources=resources,
                          opts=opts)
    if map_error:
        return ReadMapOut(map_success=False)
    return ReadMapOut(map_success=True,
                      forward_mapped=forward_mapped,
                      reverse_mapped=reverse_mapped,
                      forward_unmapped=forward_unmapped,
                      reverse_unmapped=reverse_unmapped)


def filter_expt(expt: ExptRun,
                genomes: Dict[str, Genome],
                shell: Shell = SHELL,
                resources: Resources = RESOURCES) -> int:
    """
    Filter contaminant reads form reads
    and update read references

    Args:
        phases: dict of all phases
        genomes: reference dict
        shell: exec env
        resources: compute resources

    Returns:
        error code
    """
    if not expt.contaminants:
        return 0
    error_code = 0
    for host in expt.contaminants:
        contam = genomes.get(host)
        if contam is None:
            print(f"{host}'s genome was not found, ignoring", mark='warn')
            error_code += 1
            continue
        print(f"Filtering {host}'s reads", mark='info')
        readmap = hisat_map((expt.forward, expt.reverse),
                            contam,
                            samfile=os.devnull,
                            shell=shell,
                            resources=resources,
                            opts=expt.hisat_flags)

        # Update filtered
        if not readmap.map_success:
            # mapping was unsuccessful
            print("An error occurred during mapping, skipping", mark='warn')
            error_code += 1
            continue
        expt.forward = readmap.forward_unmapped
        expt.reverse = readmap.reverse_unmapped
    return error_code


def map_to_target(expt: ExptRun,
                  genomes: Dict[str, Genome],
                  target: str,
                  shell: Shell = SHELL,
                  resources: Resources = RESOURCES) -> Optional[Path]:
    """
    Map reads to host
    and update read references

    Args:
        phases: dict of all phases
        genomes: reference dict
        shell: exec env
        resources: compute resources

    Returns:
        Path of alignment bam file

    """
    if expt.forward is None:
        raise ExptError('experiment with accesssion', expt.accession,
                        "doesn't have forward read")

    genome = genomes.get(target)
    if genome is None:
        raise TargetError(target)
    good_align = str(expt.base) + '_' + genome.sysname + '.sam'
    print(f"mapping to {target} genome", mark='info')
    readmap = hisat_map((expt.forward, expt.reverse),
                        genome,
                        samfile=good_align,
                        shell=shell,
                        resources=resources,
                        opts=expt.hisat_flags)

    # Update filtered
    if not readmap.map_success:
        # mapping was unsuccessful
        print("An error occurred during mapping, skipping", mark='warn')
        return None
    expt.forward = readmap.forward_unmapped
    expt.reverse = readmap.reverse_unmapped

    if Path(good_align.replace('.sam', '.bam')).is_file():
        good_align = Path(good_align)
    else:
        good_align = sam2bam(Path(good_align),
                             replace=True,
                             shell=shell,
                             threads=resources.max_threads)

        good_align = sort_bam(good_align,
                              good_align,
                              shell=shell,
                              threads=resources.max_threads,
                              tmp=resources.tmp)
    return good_align


def hisat_mappings(
        phases: Dict[str, Phase],
        genomes: Dict[str, Genome],
        target: str,
        shell: Shell = SHELL,
        resources: Resources = RESOURCES) -> Dict[str, Optional[Path]]:
    """
    Filter contaminant reads form reade

    Args:
        phases: dict of all phases
        shell: exec env
        resources: compute resources

    Returns:
        List of samfiles
    """
    align_handle: Dict[str, Optional[Path]] = {}
    for phase_name, phase in phases.items():
        print(f'Filtering reads for {phase_name}', mark='info')
        for expt_name, expt in phase.replicates.items():
            print(f'Processing replicate {expt_name}', mark='info')
            if filter_expt(expt=expt,
                           genomes=genomes,
                           shell=shell,
                           resources=resources):
                align_handle[f'{expt_name}:{target}'] = None
            align_handle[f'{expt_name}:{target}'] = map_to_target(
                expt=expt,
                genomes=genomes,
                target=target,
                shell=shell,
                resources=resources)
    return align_handle
