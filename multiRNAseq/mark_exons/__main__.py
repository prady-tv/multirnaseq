#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of mark-leish-exons.
#
# mark-leish-exons is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mark-leish-exons is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with mark-leish-exons.  If not, see <https://www.gnu.org/licenses/>.
#
"""
main call

"""

from pathlib import Path

from multiRNAseq.biomol import DNA
from multiRNAseq.classes import TargetConfig

from .fastq_io import FastQFile

DATA = Path(__file__).parent.parent.parent / 'tests/data/real_fastq'


def main():
    config = TargetConfig()
    fq_data = FastQFile(config=config,
                        fname=DATA / 'test_1.fastq.gz',
                        rname=DATA / 'test_2.fastq.gz')
    fq_data.spawn_analysis(term=DNA('A' * 200), min_qual=30, info_len=14)


if __name__ == '__main__':
    main()
