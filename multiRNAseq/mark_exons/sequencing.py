#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Sequencing classes
"""

from logging import warn
from typing import Optional, Sequence, Tuple, Union

import numpy as np

from ..biomol import DNA
from ..errors import BadFastqRecordError
from ..native_parallel import trim_bad


def qual2str(qual: np.ndarray, offset: int = 33) -> str:
    """
    Convert quality string to quality

    Args:
        qual: quality array
        offset: offset of quality string (33/64)

    Returns:
        string representation of quality
    """
    return ''.join((chr(pos + offset) for pos in qual))


def str2qual(qual_str: Sequence[str], offset: int = 33) -> np.ndarray:
    """
    Convert quality string to quality

    Args:
        qual_str: quality string
        offset: offset of quality string (33/64)

    Returns:
        array of quality
    """
    return np.array(list(
        ord(pos) - offset for pos in filter(
            lambda x: x not in (' ', '\n', '\r', '\t'), qual_str)),
                    dtype=np.uint8)


class FastQual():
    """
    Fastq Quality score

    Attributes:
        qual: qualities (no representation, plain phred int)
        offset: letter's offset from phred score (added to phred to get char)

    Args:
        qual_str: quality string as it appears in fastq file
        offset: offset for phred if known (typically 33|64)
    """
    def __init__(self,
                 qual: Union[Sequence[str], np.ndarray] = '',
                 offset: int = None):
        # We start by assuming offset of 64
        self.offset = offset or 64
        self._qual: Optional[np.ndarray] = None
        self._qual_str: Optional[str] = None
        if isinstance(qual, np.ndarray):
            self._qual = np.array(qual, dtype=np.int8)
        elif isinstance(qual, str):
            self._qual_str = qual
        else:  # pragma: no cover  # unused, for future
            # assume sequence
            self._qual = str2qual(qual, self.offset)

    @property
    def qual(self) -> np.ndarray:
        # This needs to be jit, because of possibility of altered offset
        if self._qual is None:
            if self._qual_str is None:  # pragma: no cover  # safety valve
                self._qual = np.array([], dtype=np.int8)
            else:
                self._qual = str2qual(self._qual_str, self.offset)
        return self._qual

    def __repr__(self) -> str:
        output = []
        for idx, nuc in enumerate(self.qual):
            if idx > 100:
                output.append("...")
                break
            output.append(f'{nuc}, ')
        return ''.join(output)

    def __iter__(self):
        """iterate over quality values"""
        return self.qual.__iter__()

    def __len__(self):
        """length of read"""
        return self.qual.size

    @property
    def off33(self) -> str:
        """
        Convert quality to a string with offset 33
        - This quality is used by sanger, Illumina 1.8+
        """
        return qual2str(self.qual, 33)

    def trim(self, start: int = 0, end: int = None):
        """
        Trim length of quality to only include a sub-Sequence
        `Remember to trim RNA sequence`

        Args:
            start: index in current sequence at which new sequence starts
            end: index in current sequence at which new sequence ends
        """
        if end is None:
            end = len(self)
        end = min(end, len(self))
        self._qual = self.qual[start:end]

    def guess_type(self, batch: Sequence[Sequence[str]] = None) -> int:
        """
        Correct Offset if evident

        Args:
            batch: list of quality strings batch to guess

        Returns:
            guessed offset

        Raises
            BadFastqRecordError: batch (and self._qual_str) is empty

        """
        # We start by assuming offset of 64 unless inferred otherwise
        _batch = [
            ord(base) for read in (batch or (self._qual_str, ))
            for base in read
        ]
        if _batch == []:
            raise BadFastqRecordError('qual_str is empty -> ')
        if min(_batch) < (self.offset - 5):
            # Solexa reports from -5
            self.offset = 33
            self._qual = None  # reset _qual
        return self.offset


class Read():
    """
    Fastq Read object
    - handles sequence and quality
    - `oop` for trimming, edge-search

    Attributes:
        seq_id: read-handle id
        mate_id: read-handle of mate (inferred from ``seq_id`` else ``seq_id``)
        seq: Nucleotide-sequence
        qual: quality values
        edge: ?is read a part of transcript edge

            - -1: read has termination sequence
            - 1: read has leader sequence
            - 0: read lacks edge sequences

    Args:
        record: record read from @ to @
        qual_type: {phred33, phred64, solexa64}

    Raises:
        BadFastqRecordError: FastqRecord is corrupted

    """
    def __init__(self, record: Tuple[Union[str, bytes], Union[str, bytes],
                                     Union[str, bytes]], offset: int):
        self.edge: int = 0
        self.seq_id: str
        self.seq: DNA
        self.qual: FastQual
        self.offset = offset
        self.seq_id, self.seq, self.qual = self.parse_record(record)
        self.mate_id = self.guess_mate_id()

    def __len__(self) -> int:
        return len(self.seq)

    def __str__(self) -> str:
        return '\n'.join((self.seq_id, self.seq.to_str(u_case=True), "+",
                          self.qual.off33)) + '\n'

    def __repr__(self) -> str:
        output = [
            f'Seq_id: {self.seq_id}', f'seq: {self.seq}', f'qual: {self.qual}',
            'edge: ' + ['None', 'Leader', 'Terminal'][self.edge]
        ]
        return "\n    ".join(output)

    def __getitem__(self, idx) -> DNA:
        """
        Subscriptable
        """
        return self.seq[idx]

    def guess_mate_id(self) -> str:
        if r"\2" in self.seq_id:
            return r'\1'.join(self.seq_id.rsplit(r'\2', 1))
        if r"\1" in self.seq_id:
            return r'\2'.join(self.seq_id.rsplit(r'\1', 1))
        return self.seq_id

    def parse_record(
        self, record: Tuple[Union[bytes, str], Union[bytes, str], Union[bytes,
                                                                        str]]
    ) -> Tuple[str, DNA, FastQual]:
        """
        Parse seq_id, seq, qual_str

        Args:
            record: record strings: seq_id, seq, qual_str

        Returns:
            Validated structured information: seq_id, sequence, quality

        Raises:
            BadNAError: Read is not of type [D|R]NA
            BadFastqRecordError: Record cannot be parsed
        """
        seq_id = record[0].decode().rstrip() if isinstance(
            record[0], bytes) else record[0].rstrip()
        if seq_id[0] != '@':
            seq_id = '@' + seq_id
        seq = DNA(record[1].decode()) if isinstance(record[1], bytes) else DNA(
            record[1])
        qual_str = record[2].decode() if isinstance(record[2],
                                                    bytes) else record[2]
        qual = FastQual(qual_str, self.offset)
        if len(seq) != len(qual):
            warn('unequal lengths of sequence- and quality- strings: all ' +
                 'such reads will be skipped')
            return '', DNA([]), FastQual([])
        return seq_id, seq, qual

    def trim(self, start: int = 0, end: int = None, reverse: bool = False):
        """
        Trim read sequence and read quality

        Args:
            start: keep read starting at
            end: keep end upto
            reverse: interpret positions from end
        """
        if end is None:
            end = len(self.seq)
        _end = (len(self.seq) - start) if reverse else end
        start = (len(self.seq) - end) if reverse else start
        self.seq.trim(start=start, end=_end)
        self.qual.trim(start=start, end=_end)

    def trim_bad(self, min_qual: int = 30):
        """
        Trim bad patches of the read.
        - If an internal bad patch is found, use longest contiguous good patch.

        Args:
            min_qual: minimum required quality of good base.

                - 0  -> 1 (100%) error
                - 10 -> 0.1 (10%) error
                - 20 -> 0.01 (1%) error
                - 30 -> 0.001 (0.1%) error
                - 40 -> 0.0001 (0.01%) error
        """
        start, end = trim_bad(self.qual.qual, min_qual=min_qual)
        self.trim(start, end)
