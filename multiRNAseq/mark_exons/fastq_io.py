#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Input/outputs on fastq-file (pairs)
"""
import time
from os import PathLike
from pathlib import Path
from typing import Dict, Optional, Tuple

from Bio.SeqIO.QualityIO import FastqGeneralIterator
from psprint import print

from ..classes import TargetConfig
from ..errors import MarkError
from ..native_parallel import find_edge
from .contexts import OptGZContext
from .pathmod import fastq_mod
from .sequencing import Read


class FastQFile():
    """
    FastQ file object

    Attributes:
        config: experimental design config
        fname: Path to file containing fastq reads (may be zipped)
        rname: Path to file containing pair- reads (may be zipped) or ``None``
        fq_h: handles for fastq files
        outfiles: handles to output files (zipped)
        read_gen: generator of read batches
        singletons: Reads whose mate hasn't been encountered
        pairs: read-pairs
        offset: offset of quality encoding

    Args:
        config: experimental design config
        fname: Path to file containing fastq reads (may be zipped)
        rname: Path to file containing pair- reads (may be zipped) or 'guess'
        offset: default=64, optional: 33

    Raises:
        BadFastqRecordError: read-files follow inconsistent quality offsets

    """
    def __init__(self,
                 config: TargetConfig,
                 fname: PathLike,
                 rname: Path = None,
                 offset=33):
        self.config = config
        self.fname = Path(fname)
        self.rname = Path(rname) if rname is not None else None
        self.fq_h = (OptGZContext(self.fname),
                     OptGZContext(self.rname) if self.rname else None)
        self.outfiles = {
            'qc_single':
            open(fastq_mod('qc_single', fpath=self.fname, gz=True), mode='wb'),
            'lead_single':
            open(fastq_mod('lead_single', fpath=self.fname, gz=True),
                 mode='wb'),
            'term_single':
            open(fastq_mod('term_single', fpath=self.fname, gz=True),
                 mode='wb'),
            'qc_1':
            open(fastq_mod('qc', fpath=self.fname, gz=True), mode='wb'),
            'lead_1':
            open(fastq_mod('lead', fpath=self.fname, gz=True), mode='wb'),
            'term_1':
            open(fastq_mod('term', fpath=self.fname, gz=True), mode='wb'),
        }
        if self.rname:
            self.outfiles = {
                **self.outfiles,
                'qc_2':
                open(fastq_mod('qc', fpath=self.rname, gz=True), mode='wb'),
                'lead_2':
                open(fastq_mod('lead', fpath=self.rname, gz=True), mode='wb'),
                'term_2':
                open(fastq_mod('term', fpath=self.rname, gz=True), mode='wb'),
            }

        self.singletons: Dict[str, Read] = {}
        self.offset = offset

    def __repr__(self) -> str:

        return f'''
        forward: {self.fname}
        reverse: {self.rname}
        singletons: {len(self.singletons)}
        '''

    def dump_singletons(self, read: Read = None):
        """
        Write read to cognate files

        - These reads have their mates missing
          probably because they had bad quality

        """
        single_bank = (
            read, ) if read is not None else self.singletons.values()
        for read in single_bank:
            if read.edge == 1:
                # leader
                self.outfiles['lead_single'].write(str(read).encode())
            elif read.edge == -1:
                # term
                self.outfiles['term_single'].write(str(read).encode())
            else:
                # ordinary
                self.outfiles['qc_single'].write(str(read).encode())

    def pair_up(self,
                read: Read = None,
                rev: bool = False) -> Tuple[Optional[Read], Optional[Read]]:
        """
        Search read-pairs in ``singletons`` and populate ``good_reads``
        """
        if read is None:
            return None, None
        if self.rname is None:
            # Reads are not paired ends/mate-pairs
            return read, None
        if read.mate_id in self.singletons:
            mate = self.singletons[read.mate_id]
            del self.singletons[mate.seq_id]
            if '\1' in mate.seq_id or rev:
                return mate, read
            return read, mate
        # remember
        self.singletons[read.seq_id] = read
        return None, None

    def write_down(self, read: Read = None, rev: bool = False):
        """
        Write read to cognate files
        """
        read, mate = self.pair_up(read, rev)
        if read is None and mate is None:
            return
        if mate is None:
            self.dump_singletons(read)
            return
        if read.edge == 1 or mate.edge == 1:
            # leader
            self.outfiles['lead_1'].write(str(read).encode())
            self.outfiles['lead_2'].write(str(read).encode())
        elif read.edge == -1 or mate.edge == -1:
            # term
            self.outfiles['term_1'].write(str(read).encode())
            self.outfiles['term_2'].write(str(read).encode())
        else:
            # ordinary
            self.outfiles['qc_1'].write(str(read).encode())
            self.outfiles['qc_2'].write(str(read).encode())

    def process_read(self, read: Read) -> Optional[Read]:
        """
        Pass read through trimming and edge-search.
        Commits changes to referred read object.

        Args:
            *args: arguments tuple

                - read: Read: read to process
                - min_qual: int: minimum base quality to be considered good
                - lead: DNA: leader DNA sequence
                - term: DNA: terminal DNA sequence
        """
        read.trim_bad(self.config.min_qual)
        if len(read) < self.config.min_qual:
            return None
        read.edge, start, end = find_edge(read=read.seq.seq,
                                          lead=self.config.leader.seq,
                                          rev_lead=self.config.leader.rev,
                                          term=self.config.term.seq,
                                          rev_term=self.config.term.rev,
                                          inform=self.config.info_len,
                                          err_rate=self.config.mismatch)
        if end - start < self.config.min_qual:
            return None
        read.trim(start, end)
        return read

    def spawn_analysis(self, **kwargs):
        """
        Spawn parallel analysis, classification and outputs
        - draw a chunk of reads
        - trim bad quality
        - look for leader/terminal
        - classify and register to output files

        Args:
            **kwargs: passed to self.``fork_processes``
                - lead: DNA: leader sequence of transcripts (mark exons)
                - term: DNA: termination sequence of transcripts (mark exons)
                - min_qual: int: minimum quality of base to be considered good
                - max_seq: int: only process these many sequences
        """
        try:
            self.config.update(kwargs)
            seqs_left = kwargs.get('max_seq', -1)
            fq_g = [
                FastqGeneralIterator(self.fq_h[0].handle),
                FastqGeneralIterator(self.fq_h[1].handle)
                if self.rname else None
            ]
            reads_available = [True, bool(self.rname)]
            s_time = time.time()
            info_batch = 1000000
            while seqs_left != 0 and any(reads_available):
                if seqs_left % info_batch == 0:
                    print(f'{abs(seqs_left)} sequences', mark='info')
                    print(f'{info_batch / (time.time() - s_time):0.4f}seq/sec')
                    s_time = time.time()
                for orient in [0, 1]:
                    fq_r = fq_g[orient]
                    if reads_available[orient]:
                        try:
                            read = Read(next(fq_r), offset=self.offset)
                            if len(read) == 0:
                                # read is trivial
                                continue
                            self.write_down(self.process_read(read),
                                            rev=not (bool(orient)))
                        except (StopIteration, EOFError):
                            reads_available[orient] = False
                seqs_left -= 1
            else:
                self.fq_h[0].close()
                if self.fq_h[1] is not None:
                    self.fq_h[1].close()
                self.dump_singletons()
                for open_file in self.outfiles.values():
                    open_file.close()
        except OSError:
            raise MarkError(self.fname)
