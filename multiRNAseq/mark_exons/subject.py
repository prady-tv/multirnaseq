#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Subject of analysis defined from command line
    - Quick reference `guide`
'''

from typing import Any, Dict, Optional

from ..biomol import DNA


class subject():
    '''
    Subject of analysis

    Attributes:
        lead: DNA: leader sequence
        term: DNA: terminal sequence
        min_qual: int: minimum quality to be considered good
        read_min: int: minimum length of read to be acceptibly long
        inform: float: minimum informative sequence
        mismatch: float: mismatches allowed during alignment

    Args:
        **kwargs: attributes to set away from default
        all others are ignored
    '''
    def __init__(self, **kwargs):
        self.lead: Optional[DNA] = None
        self.term: Optional[DNA] = None
        self.min_qual = 30
        self.read_min = 20
        self.inform = 14
        self.mismatch = 0.01
        self.update(**kwargs)

    def update(self, **kwargs):
        '''
        Update attributes
        - Redefine attributes

        Args:
            **kwargs:
                - lead: DNA: leader sequence
                - term: DNA: terminal sequence
                - min_qual: int: minimum quality to be considered good
                - read_min: int: minimum length of read to be acceptibly long
                - inform: float: minimum informative sequence
                - mismatch: float: mismatches allowed during alignment

            all others are ignored
        '''
        default: Dict[str, Any] = {}
        default['lead'] = None
        default['term'] = None
        default['min_qual'] = 30
        default['read_min'] = 20
        default['inform'] = 14
        default['mismatch'] = 0.01
        for key in kwargs:
            if hasattr(self, key):
                if getattr(self, key) == default[key]:
                    setattr(self, key, kwargs[key])
