#!/usr/bin/env python3
# -*- coding:utf-8; mode:python -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of mark=leish-exons.
#
# mark=leish-exons is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# mark=leish-exons is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with mark=leish-exons.  If not, see <https://www.gnu.org/licenses/>.
#
"""
handle files, objects, etc, preferably under contexts
"""

import gzip
import os
from pathlib import Path
from typing import IO, TextIO, Union


class OptGZContext():
    """
    Context to handle [zipped] files.
    Parses file-name[.gz] to infer whether file is zipped.
    Behaves like a normal 'open(file)' context manager unless zipped.

    Args:
        filename: path to file
        mode: mode to write/read (added with 't' for zipped)
        **kwargs: all are passed to the the cognate context manager

    Attributes:
        handle: handle to perform IO operations
        closed: returns ``closed`` attribute of ``handle``
    """
    def __init__(self, filename: os.PathLike, mode: str = 'r', **kwargs):
        if mode[-1] != 'b':
            # not in binary mode
            mode += "t"
        self.handle: Union[gzip.GzipFile, TextIO, IO[str]]
        if Path(filename).suffix == '.gz':
            # file is gzipped
            self.handle = gzip.open(filename, mode=mode, **kwargs)
        else:
            self.handle = open(filename, mode=mode, **kwargs)

    def __enter__(self) -> Union[gzip.GzipFile, TextIO, IO[str]]:
        """
        Enter context
        """
        return self.open()

    def __exit__(self, *_):
        """
        Close context
        """
        self.close()

    def open(self) -> Union[gzip.GzipFile, TextIO, IO[str]]:
        """
        Call method open
        """
        return self.handle

    def close(self):
        """
        Call method close
        """
        self.handle.close()
