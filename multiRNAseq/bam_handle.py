#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Handle sam/bam files
"""

import os
import shutil
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Union

from psutil import virtual_memory

from .shell import Shell

SHELL = Shell()
CORES = len(os.sched_getaffinity(0))


def sam2bam(in_sam: os.PathLike,
            out_bam: os.PathLike = None,
            replace: bool = False,
            shell: Shell = SHELL,
            threads: int = 1) -> Path:
    """
    Convert samfile to bamfile

    Args:
        in_sam: samfile
        out_bam: outfile (default: .sam->.bam)
        replace: replace sam file by bam file
        shell: exec env
        threads: multithreading

    Returns:
        bam file path
    """
    in_sam = Path(in_sam)
    if out_bam is None:
        out_bam = in_sam.with_suffix('.bam')
    else:
        out_bam = Path(out_bam)

    threads = min(CORES, threads)

    s2b_out = shell.process_comm('samtools',
                                 'view',
                                 '-@',
                                 threads,
                                 '-b',
                                 '-o',
                                 out_bam,
                                 in_sam,
                                 p_name='s2b',
                                 fail='notify')

    if replace and (s2b_out is not None):
        in_sam.unlink()

    return out_bam


def combine_bam(*in_bams: os.PathLike,
                combine_bam: os.PathLike = None,
                shell: Shell = SHELL,
                tmp: Path = None,
                threads: int = 1,
                sort: bool = True) -> Path:
    """
    Combine BAM files into one

    Args:

        *in_bams: input bam file paths
        combine_bam: name of output bam (default: cat all in first's parent) (this may be one of inputs)
        shell: exec env
        sort: sort combined file?
        threads: multithreading
        tmp: location for bam sort temporary files

    Returns:
        handle for output file
    """
    in_bam_l = list(Path(aln) for aln in in_bams)
    if combine_bam is None:
        out_parent = in_bam_l[0].parent
        combine_bam = out_parent / ('_'.join(aln.stem
                                             for aln in in_bam_l) + '.bam')
    else:
        combine_bam = Path(combine_bam)

    threads = min(CORES, threads)

    with NamedTemporaryFile('w+b', dir=tmp) as temp_combine:
        # combine
        shell.process_comm('samtools',
                           'merge',
                           '-@',
                           threads,
                           '-f',
                           temp_combine.name,
                           *in_bam_l,
                           p_name='b+b=b',
                           fail='notify')
        if sort:
            _ = sort_bam(Path(temp_combine.name),
                         Path(temp_combine.name),
                         shell=shell,
                         threads=threads,
                         tmp=tmp)

        shutil.copy(temp_combine.name, combine_bam)

    return combine_bam


def sort_bam(in_bam: os.PathLike,
             out_bam: os.PathLike = None,
             shell: Shell = SHELL,
             threads: int = 1,
             tmp: Path = None) -> Path:
    """
    Sort bam file

    Args:
        in_bam: input bam file
        out_bam: output file (this may be same as in_bam)
        shell: exec env
        threads: multithreading
        tmp: location for bam sort temporary files

    Returns:
        handle to sorted output
    """
    in_bam = Path(in_bam)
    if out_bam is None:
        out_bam = (in_bam.parent / in_bam.name).with_suffix('.sorted.bam')
    else:
        out_bam = Path(out_bam)

    if tmp is None:
        tmp == out_bam.parent

    threads = min(CORES, threads)
    mem_per_thread = str(guage_mem(threads=threads, m_unit='mb')) + 'M'

    with NamedTemporaryFile('w+b', dir=tmp) as temp_sort:
        shell.process_comm('samtools',
                           'sort',
                           '-@',
                           threads,
                           '-m',
                           mem_per_thread,
                           '-T',
                           tmp,
                           '-o',
                           temp_sort.name,
                           in_bam,
                           p_name='b_sort',
                           fail='notify')
        shutil.copy(temp_sort.name, out_bam)
    return out_bam


def guage_mem(head_space: float = 0.2,
              threads: int = 1,
              m_unit: Union[str, int] = 1) -> int:
    """
    Guage available memory

    Args:
        head_space: leave fraction untouched
        threads: multithreading
        m_unit: units of memory: kilo, mega, 1024, etc

    Returns:
        (1-head_space) of available free RAM memory // threads in m_unit

    Raises:
        ValueError: bad denom_value
    """
    if head_space > 1:
        # may be percentage
        head_space /= 100
    if isinstance(m_unit, str):
        try:
            m_unit = int(m_unit)
        except ValueError:
            # not a number
            m_unit = m_unit.lower().replace(' ', '').replace('bytes', '')
            if m_unit in ('tb', 'tera', 't'):
                m_unit = 0x1_00000_00000
            elif m_unit in ('gb', 'giga', 'g'):
                m_unit = 0x400_00000
            elif m_unit in ('mb', 'mega', 'm'):
                m_unit = 0x1_00000
            elif m_unit in ('kb', 'kilo', 'k'):
                m_unit = 0x400
            else:
                raise ValueError(f'Incorrect m_unit')

    return int(virtual_memory().available * (1 - head_space) /
               (threads * m_unit))
