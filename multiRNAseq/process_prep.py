#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
pre-processing

    - DOWNLOAD
    - TRIM
    - INDEX
    - ANNOTATE

"""

import sys
from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from typing import Dict, List, Optional, Tuple

import psutil
from psprint import print

from .classes import ExptRun, Genome, Phase
from .errors import ExptError, ExternalError
from .pathmod import fastq_mod
from .shell import Shell

SHELL = Shell()
CORES = len(sched_getaffinity(0))


def pull_genome(genome: Genome, shell: Shell = SHELL) -> List[str]:
    """
    Pull fasta (download/link/assign)

    Args:
        genome: genome to download
        shell: exec shell environment

    Returns:
        List of Genome: keys with error
    """
    error_key: List[str] = []
    genome.root.mkdir(parents=True, exist_ok=True)
    for key, value in genome._raw_paths.items():
        dest_path: Path = getattr(genome, key)
        if value is None:
            # leave untouched
            continue
        if dest_path.is_file():
            # file exists
            continue
        if Path(value) == dest_path:
            # Path to an already downloaded local file
            continue
        if Path(value).is_file():
            # Path to a local file
            dest_path.symlink_to(value)
            continue
        # not a local file
        print(f"Downloading {value}", mark="info")
        aria_cmd = [
            'aria2c', '-c', value, '-d', genome.root, '-o', dest_path.name
        ]
        if key == "hisat2_index":
            # we assume that this file is tar-gzipped
            aria_cmd[-1] += ".tar.gz"
            shell.process_comm(*aria_cmd,
                               p_name='aria2c download',
                               fail='notify')
            shell.process_comm('tar',
                               '-C',
                               genome.root,
                               '-xz',
                               '-f',
                               dest_path.parent / (dest_path.name + '.tar.gz'),
                               p_name='untar decompress',
                               fail='notify')
        elif value[-3:] != ".gz":
            # plain file download
            shell.process_comm(*aria_cmd,
                               p_name='aria2c download',
                               fail='notify')
        else:
            aria_cmd[-1] += ".gz"
            shell.process_comm(*aria_cmd,
                               p_name='aria2c download',
                               fail='raise')
            shell.process_comm('gzip',
                               '-d',
                               dest_path.parent / (dest_path.name + ".gz"),
                               p_name='decompress',
                               fail='raise')
        if dest_path is not None and shell.exec_env.execute:
            if not dest_path.is_file():
                error_key.append(f'{genome.name}: {key}')
    return error_key


def pull_sra(expt: ExptRun, shell: Shell = SHELL):
    """
    Pull files
    and update read references

    Args:
        expt: rnaseq experiment run to pull
        shell: exec_env

    Raises:
        ExternalError
    """
    # salvage
    if any(
        (expt.parent_dir / (expt.accession + fq_orient + ".fastq")).is_file()
            for fq_orient in ("", "_1", "_2")):
        print(f"{expt.accession} files are already downloaded", mark='info')
        return
    # download
    shell.process_comm('prefetch',
                       expt.accession,
                       p_name='SRA download',
                       fail='raise')
    # dump
    shell.process_comm('fastq-dump',
                       expt.accession,
                       '-O',
                       expt.parent_dir,
                       '--split-3',
                       p_name='fastq-dump',
                       fail='raise')

    # compress
    for fq_orient in ("", "_1", "_2"):
        dump_path = Path(
            str(expt.parent_dir) + expt.accession + fq_orient + ".fastq")
        if dump_path.is_file():
            shell.process_comm('gzip',
                               dump_path,
                               p_name='compress reads',
                               fail='raise')


def pull_phase(phase: Phase, shell: Shell = SHELL) -> List[str]:
    """
    Pull phases

    Args:
        phase: phase object to pull
        shell: exec_env

    Returns:
        List of accessions of relicates that threw error

    """
    err_acc: List[str] = []
    phase.root.mkdir(parents=True, exist_ok=True)
    for accession, expt in phase.replicates.items():
        if any((phase.root / (accession + read_ori + ".fastq.gz")).is_file()
               for read_ori in ("_1", "_2", "")):
            print(f'Found downloaded {accession}')
            continue
        try:
            print(f"Downloading {accession}")
            pull_sra(expt, shell=shell)
            expt.which_reads()
        except (ExternalError, OSError) as err:
            print(err, mark='err', file=sys.stderr)
            err_acc.append(accession)
    return err_acc


def pull_rna(phases: Dict[str, Phase], shell: Shell = SHELL) -> List[str]:
    """
    Initiate all necessary downloads

    Args:
        genomes: Genomes used in analysis
        phases: Phases used in analysis
        shell: analysis shell environment

    Returns:
        List of phases that threw error
    """
    err_acc: List[str] = []
    for condition, phase in phases.items():
        print(f"Downloading data for {condition}", mark='info')
        err_acc.extend(pull_phase(phase, shell=shell))
    return err_acc


def ht2_index(genome: Genome, shell: Shell = SHELL, threads: int = 1) -> int:
    """
    Index Hisat2 if necessary

    Args:
        genome: Genome to optionally index

    Returns:
        error code
    """
    error_code = 0
    print(f"Indexing data for {genome.name}", mark='info')
    if genome.hisat2_index.with_suffix('.1.ht2').is_file():
        # index file(s) exist
        print('Index files found, using them', mark='info')
        return error_code
    hisat2_index_cmd = [
        'hisat2-build', '-p', threads, genome.fasta, genome.hisat2_index
    ]
    if genome.hisat2_splicesites.is_file():
        if (psutil.virtual_memory().free // (2**30)) > 200:
            hisat2_index_cmd.insert(1, '--ss')
            hisat2_index_cmd.insert(2, str(genome.hisat2_splicesites))
        else:
            print('Insufficient memory: Not indexing hisat2 with splice-sites',
                  mark='warn')
    if genome.hisat2_exons.is_file():
        if (psutil.virtual_memory().free // (2**30)) > 200:
            hisat2_index_cmd.insert(1, '--exon')
            hisat2_index_cmd.insert(2, str(genome.hisat2_exons))
        else:
            print('Insufficient memory: Not indexing hisat2 with exons',
                  mark='warn')
    shell.process_comm(*hisat2_index_cmd, fail='notify')
    return error_code


def call_gffread(args: Tuple[Path, Path, Shell]) -> Optional[str]:
    """
    Worker process to call gffread -T gff > gtf

    Args:
        args: wrapped arguments

            - gff_path: path to gff file
            - gtf_path: path to gtf file
            - shell: exec_env

    returns:
        err_code
    """
    gff_path, gtf_path, shell = args
    if gtf_path.is_file():
        # already exists
        return
    try:
        shell.process_comm('gffread',
                           '-T',
                           '-o',
                           gtf_path,
                           gff_path,
                           fail='raise')
    except ExternalError:
        gff_path.name


def make_gtf(genomes: Dict[str, Genome],
             threads: int = 1,
             shell: Shell = SHELL) -> List[str]:
    """
    Convert gff3 records into gtf records

    Args:
        genomes: genomes used for analysis
        shell: Analysis shell environment
        threads: threads to use for hisat indexing

    Returns:
        number of failures
    """
    pool = Pool(min(CORES, threads))
    a_map = pool.map_async(call_gffread, ((genome.gff, genome.gtf, shell)
                                          for genome in genomes.values()))
    # Ignoring type, since we are specifically filtering out ``None``
    return list(filter(lambda x: x is not None, a_map.get()))  # type: ignore


def index_genomes(genomes: Dict[str, Genome],
                  shell: Shell = SHELL,
                  threads: int = 1) -> List[str]:
    """
    Run Hisat2 indexing on genomes

    Args:
        genomes: Genomes used in analysis
        shell: Analysis shell environment
        threads: threads to use for hisat indexing

    Returns:
        List of organisms whose genome-download had errors
    """
    err_org: List[str] = []
    for organism, genome in genomes.items():
        print(f"Downloading data for {organism}", mark='info')
        # Download
        err_org.extend(pull_genome(genome, shell=shell))
        # Index
        if ht2_index(genome, shell=shell, threads=threads):
            err_org.append(organism)
    # Create GTF from GFF file
    err_org.extend(make_gtf(genomes, threads=threads, shell=shell))
    return err_org


def _trim_expt(expt: ExptRun, shell: Shell = SHELL, threads: int = 1) -> int:
    """
    Trim all RNAseq with trim_galore,
    and update read references

        - SL-Seq: 5' leader (21 bp) of _2 with cutadapt

    Run FastQC after trimming

    Args:
        expt: replicate experiment
        shell: Analysis shell environment
        threads: threads to use for hisat indexing

    Returns:
        err_code

    """
    if expt.forward is None:
        return 1
    trimgalore_cmd = [
        'trim_galore', '-o', expt.forward.parent, '-j',
        min(threads, CORES), expt.forward
    ]
    if expt.reverse is not None:
        # run trimgalore without -p flag
        trimgalore_cmd.insert(-1, '--paired')
        trimgalore_cmd.append(expt.reverse)
    _ = shell.process_comm(*trimgalore_cmd, p_name='auto_trim', fail='raise')
    if expt.reverse is not None:
        fastq_mod('1', 'val', fpath=expt.forward,
                  fq=True).replace(fastq_mod('trimmed', fpath=expt.forward))
        fastq_mod('2', 'val', fpath=expt.reverse,
                  fq=True).replace(fastq_mod('trimmed', fpath=expt.reverse))
    else:
        fastq_mod('trimmed', fpath=expt.forward, fq=True).replace(
            fastq_mod('trimmed', fpath=expt.forward, fq=False))
    if expt.seqtype == 'sl' and expt.reverse is not None:
        # trim further 21 nucleotides from forward and reverse reads
        cutadapt_cmd = [
            'cutadapt', '-j',
            min(threads, CORES), '-u', 21, '-m', 20, '--trim-n', '-o',
            fastq_mod('sl_cut', fpath=expt.forward), '-p',
            fastq_mod('sl_cut', fpath=expt.reverse),
            fastq_mod('trimmed', fpath=expt.forward),
            fastq_mod('trimmed', fpath=expt.reverse)
        ]
        shell.process_comm(*cutadapt_cmd, p_name='trim_sl', fail='raise')
        # rename to standard
        fastq_mod('trimmed', fpath=expt.forward).replace(
            fastq_mod('sl_uncut', fpath=expt.forward))
        fastq_mod('sl_cut', fpath=expt.forward).replace(
            fastq_mod('trimmed', fpath=expt.forward))
        fastq_mod('trimmed', fpath=expt.reverse).replace(
            fastq_mod('sl_uncut', fpath=expt.reverse))
        fastq_mod('sl_cut', fpath=expt.reverse).replace(
            fastq_mod('trimmed', fpath=expt.reverse))
    # now, forward and reverse(if available) trimmed reads
    # are named as SRRXXXXXXXX_trimmed_[12].fastq.gz
    assert expt.forward is not None

    # make them the defaults
    # and run a qc on them
    expt.forward = fastq_mod('trimmed', fpath=expt.forward)
    shell.process_comm('fastqc',
                       '-t',
                       min(CORES, threads),
                       expt.forward,
                       p_name='fastqc trimmed',
                       fail='notify')
    if expt.reverse is not None:
        expt.reverse = fastq_mod('trimmed', fpath=expt.reverse)
        shell.process_comm('fastqc',
                           '-t',
                           min(CORES, threads),
                           expt.reverse,
                           p_name='fastqc trimmed',
                           fail='notify')
    return 0


def trim(phases: Dict[str, Phase],
         shell: Shell = SHELL,
         threads: int = 1) -> List[str]:
    """
    Trim all RNAseq with trim_galore,
    and update read references

        - SL-Seq: 5' leader (21 bp) of _2 with cutadapt

    Run FastQC after trimming

    Args:
        phases: phases used for analysis
        shell: Analysis shell environment
        threads: threads to use for hisat indexing

    Returns:
        List of accession numbers/phases that threw error

    """

    err_acc: List[str] = []
    for name, phase in phases.items():
        print(f'Trimming {name}', mark='info')
        try:
            for expt in phase.replicates.values():
                print('Processing replicate with accession number',
                      str(expt.accession),
                      mark='info')
                if expt.forward is None:
                    raise ExptError('Experiment with accesssion',
                                    expt.accession,
                                    "doesn't have forward read", '',
                                    'Please report to author')

                # check if trimmed file(s) are already available
                trimmed_f = fastq_mod('trimmed', fpath=expt.forward)
                trimmed_r = (fastq_mod('trimmed', fpath=expt.reverse)
                             if expt.reverse is not None else None)
                if trimmed_f.is_file() and (trimmed_r is None
                                            or trimmed_r.is_file()):
                    # already trimmed
                    print("Found a 'trimmed' file, using it", mark='info')
                    # update reference
                    expt.forward = trimmed_f
                    expt.reverse = trimmed_r
                    continue

                if _trim_expt(expt, shell=shell, threads=threads):
                    err_acc.append(expt.accession)
        except (ExternalError, OSError) as err:
            print(err, mark='err', file=sys.stderr)
            err_acc.append(name)
    return err_acc


def prepare(genomes: Dict[str, Genome],
            phases: Dict[str, Phase],
            shell: Shell = SHELL,
            threads: int = 1) -> List[str]:
    """
    Performs the following actions:

        - Download genomes
        - Index genomes
        - Download rnaseq data
        - Trim reads

            - trimgalore for all reads
            - 21 Hard clip corresponding to 'SL' of second in pair for SL-Seq

    Finally, for each phase->expt,
    `expt.forward` and `expt.reverse` are set to `suitable mappable reads`

    Args:
        genomes: Reference genomes
        phases: Biological phases
        threads: resources
        shell: exec env

    Returns:
        List of preparatory handles with errors
"""
    if (genomes_prep_error := index_genomes(genomes,
                                            shell=shell,
                                            threads=threads)):
        return genomes_prep_error
    if (rnaseq_prep_error := pull_rna(phases, shell)):
        return rnaseq_prep_error
    if (trim_err := trim(phases=phases, shell=shell, threads=threads)):
        return trim_err
    return []
