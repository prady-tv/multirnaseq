#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
multiRNAseq's defined errors
"""

import os


class MultiRNAseqError(Exception):
    """
    Base error for multiRNAseq(Exception):
    """
    def __init__(self, *args):
        super(Exception, self).__init__(*args)


class DesignFileNotFoundError(FileNotFoundError, MultiRNAseqError):
    """
    Supplied Design file-name does not exist
    """
    def __init__(self, *args):
        super(MultiRNAseqError, self).__init__(*args)


class FormatError(MultiRNAseqError):
    """
    Bad format of input file
    """
    def __init__(self, fname, *args):
        # TODO: make this the super-class of DesignFileFormatError
        super(MultiRNAseqError,
              self).__init__(f"Error in parsing file {fname}\n", *args)


class DesignFileFormatError(MultiRNAseqError):
    """
    Bad format of design file
    """
    def __init__(self, key):
        super(MultiRNAseqError,
              self).__init__(f"Expected section: {key} not found")


class DesignFileValueError(ValueError, MultiRNAseqError):
    """
    Bad format of design file
    """
    def __init__(self, key, value):
        super(MultiRNAseqError,
              self).__init__(f"Bad value '{value}' for {key}")


class DependencyError(FileNotFoundError, MultiRNAseqError):
    """
    A dependency is missing
    """
    def __init__(self, dep: str):
        super(MultiRNAseqError,
              self).__init__(f"Dependency {dep} required, unsatisfied")


class BadNAError(MultiRNAseqError):
    """
    Bad DNA/RNA sequence
    """
    def __init__(self, base: str):
        super(
            MultiRNAseqError,
            self).__init__(f'Bad Sequence for nucleic acid (DNA/RNA): {base}')


class OverHangError(MultiRNAseqError):
    """
    DNA overhangs don't match
    """
    def __init__(self):
        super(MultiRNAseqError, self).__init__(f'Overhangs: do not match')


class ExternalError(MultiRNAseqError):
    """
    External dependency program failed with error
    """
    def __init__(self,
                 *cmd: str,
                 retcode: int = 1,
                 stderr: str = '',
                 stdout: str = ''):
        super(MultiRNAseqError, self).__init__(f'''
        External dependency {cmd[0]} exitted with error.
        command: {cmd}

        returned: {retcode}

        output:

        {stdout}

        Error message:

        {stderr}

        ''')


class TargetError(MultiRNAseqError):
    """
    Designed target is not found or is ``None`` (unset)
    """
    def __init__(self, target: str = None):
        super(MultiRNAseqError, self).__init__(f'''
        Target genome is set to "{target}"

        Confirm that it is not ``None`` and that it is included in *genomes*
        ''')


class BadFastqRecordError(MultiRNAseqError):
    """
    Args:
        handle: identifier for fastq record
    """
    def __init__(self, handle: str):
        super(MultiRNAseqError,
              self).__init__(f'Fastq record {handle} has corrupted format.')


class MarkError(MultiRNAseqError):
    """
    Error while marking exon-edge-reads
    """
    def __init__(self, target=os.PathLike):
        super(MultiRNAseqError, self).__init__(f'''
        Error locating exon-edge reads in {str(target)}
        ''')


class ExptError(MultiRNAseqError):
    """
    Experiment is incompletely formatted
    """
    def __init__(self, *args: str):
        super(MultiRNAseqError, self).__init__('\n'.join(args))
