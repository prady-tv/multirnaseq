#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Use Hisat2 to mark reads to an artificial subject created from leader and term
"""

from pathlib import Path
from typing import Optional, Tuple

from psprint import print

from . import CONFIG
from .biomol import DNA
from .classes import Resources
from .read_config import Design
from .shell import Shell

SHELL = Shell()


def create_pseubject(
        lead: DNA = DNA(''),
        term: DNA = DNA(''),
        resources: Resources = CONFIG.resources,
        max_len: int = 400) -> Tuple[Optional[Path], Optional[Path]]:
    """
    Create a pseudo-subject of length ``max_len`` of the form:
    * '...N' + leader OR term + 'N...'
    and write it to standard location of genomes/pseudo/temp/(lead|term).fna
    If a subject file exists, it will be overwritten

    Args:
        lead: leader DNA
        term: terminator DNA
        resources: ``Resources`` object
        max_len: maximum expected read length

    Returns: error code

        - 0 if both lead and term subject files could be created
        - 1 if lead subject was not created
        - 2 if term subject was not created
        - 3 if neither was created
    """

    lead_file, term_file = None, None
    pseudo_root = resources.genome / 'pseudo/temp'
    if len(lead) != 0:
        pseudo_lead = lead.ligate(DNA('N' * max(0, (max_len - len(lead)))))
        pseudo_root.mkdir(parents=True, exist_ok=True)
        lead_file = pseudo_root / 'lead.fna'
        with open(lead_file, 'w') as out_file:
            out_file.write(">leader pseudo-genome for hisat mapping\n")
            out_file.write(pseudo_lead.to_str(u_case=True) + '\n')
    if len(term) != 0:
        pseudo_term = DNA('N' * max(0, (max_len - len(term)))).ligate(term)
        pseudo_root.mkdir(parents=True, exist_ok=True)
        term_file = pseudo_root / 'term.fna'
        with open(term_file, 'w') as out_file:
            out_file.write(">term pseudo-genome for hisat mapping\n")
            out_file.write(pseudo_term.to_str(u_case=True) + '\n')
    return lead_file, term_file


def ht2_id_subject(genome: Path, design: Design, shell: Shell = SHELL) -> int:
    """
    HT2 index ``subject`` if it does not exist, base: 'subject<w/o extension>'

    Args:
        subject: path of subject to index

    Returns:
        error code
    """
    err_code = 0
    if (genome.parent / (genome.name + '.1.ht2')).is_file():
        return err_code
    shell.process_comm('hisat2-build', '-p', design.resources.max_threads,
                       genome, genome.stem)
    return err_code


def map_edge_reads(lead_file: Path,
                   term_file: Path,
                   design: Design,
                   shell: Shell = SHELL) -> int:
    """
    map reads to a pseudo subject of ``lead`` and ``term``

    unmapped reads are written with epithet "internal"
    mapped reads are written with epithet "lead" or "term" respectively

    Returns:
        number of analyses that threw and error
    """
    err_code = 0
    for ph_name, phase in design.phases.items():
        print(f'Processing {ph_name} for edge-reads', mark='info')
        for accession, expt in phase.replicates.items():
            if not expt.forward:
                # no reads in this experiment
                continue
            print(f'Scanning {accession} for edge-reads', mark='info')
            # TODO: Construct HiSAT2 command
            hisat_cmd = []
            if expt.reverse:
                # scan leader first
                if shell.process_comm(*hisat_cmd) is None:
                    err_code += 1
            else:
                # scan leader first
                if shell.process_comm(*hisat_cmd) is None:
                    err_code += 1
    return err_code
