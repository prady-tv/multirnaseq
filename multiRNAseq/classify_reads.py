#!/usr/bin/env python3
# -*- coding: utf-8; mode: python; -*-
# Copyright © 2021 pradyumna
#
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq. If not, see <https://www.gnu.org/licenses/>.
#
"""
Classify reads into

    - leader-containing | terminal-containing
    - mate pairs | singletons

"""

import os
from multiprocessing import Pool
from typing import Tuple

from .classes import ExptRun, TargetConfig
from .errors import ExternalError, MarkError
from .mark_exons.fastq_io import FastQFile
from .mark_exons.pathmod import fastq_mod
from .read_config import Design
from .shell import Shell

SHELL = Shell()


def _worker(args: Tuple[TargetConfig, ExptRun, Shell]) -> int:
    config, expt, shell = args
    print("processing", expt.accession)
    if expt.forward is None:
        return 2
    try:
        fq_data = FastQFile(config=config,
                            fname=expt.forward,
                            rname=expt.reverse)
        fq_data.spawn_analysis()
        for read_class in ('lead', 'term', 'qc', 'lead_single', 'term_single',
                           'qc_single'):
            class_f = fastq_mod(read_class, fpath=expt.forward, gz=False)
            if class_f.is_file():
                shell.process_comm('gzip', class_f, p_name='zip', fail='raise')
            if not expt.reverse:
                continue
            class_r = fastq_mod(read_class, fpath=expt.reverse, gz=False)
            if class_r.is_file():
                shell.process_comm('gzip', class_r, p_name='zip', fail='raise')
    except (MarkError, ExternalError):
        return 2
    return 0


def get_edges(design: Design, shell: Shell = SHELL) -> int:
    """
    Analyse all files and classify them for exons

    Args:
        design: config design

    Returns:
        error code
    """
    # spawn analysis of parallel processes
    err_code = 0
    for phase_name, phase in design.phases.items():
        print(f'Marking reads contianing exons for phase {phase_name}')
        n_parallel = min(
            len(os.sched_getaffinity(0)) // 3, design.resources.max_threads,
            len(phase.replicates))
        print(f'spawning parallel analyses with {n_parallel} threads')
        pool = Pool(n_parallel)
        a_map = pool.map_async(_worker,
                               ((design.vals, expt, shell)
                                for expt in phase.replicates.values()))
        results = a_map.get()
        for r in results:
            err_code += r
    return err_code
