#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Biomolecules: Currently, only nucleic acids
"""

from typing import Optional, Sequence, Tuple, Union

import numpy as np

from .biocode import _MASK, _N, _RNA, _T, CODE, DECODE
from .errors import BadNAError, OverHangError
from .native_parallel import revcomp


def encode(nuc_letters: Sequence[str],
           is_RNA: bool = False,
           strict: bool = False) -> np.ndarray:
    """
    Encode nucleotide sequence to nucbit form

    Args:
        nuc_letters: nucleotide sequence(letters)
        is_RNA: Assume RNA sequence
        strict: fail upon encountering bad nucleotides (otherwise calls it 'n')

    Returns:
        array of corresponding nuc_bits
    """
    wsp = (' ', '\r', '\n', '\t')
    if strict:
        for n in nuc_letters:
            if n not in (*CODE, *wsp):
                raise BadNAError('')
    nuc_seq = np.array(
        [CODE.get(n, _N | _MASK) for n in nuc_letters if n not in wsp],
        dtype=np.uint8)
    if np.any(nuc_seq & _RNA) or is_RNA:
        nuc_seq = (nuc_seq | _RNA).astype(np.uint8)
    return nuc_seq


def decode(nuc_seq: np.ndarray) -> str:
    """
    Encode nucleotide sequence to nucbit form

    Args:
        nuc_seq:  nucleotide sequence(nuc_bits)

    Returns:
        array of corresponding nucleotide sequence(letters)
    """
    nuc_letters = []
    for nuc in nuc_seq:
        mask_bit = nuc & _MASK
        if nuc & _N == _T and nuc & _RNA:
            nuc_letters.append(DECODE.get(nuc & _N | _RNA | mask_bit) or 't')
        else:
            nuc_letters.append(DECODE.get(nuc & _N | mask_bit) or 'n')
    return ''.join(nuc_letters)


class DNA():
    """
    Class to handle Nucleic Acid

    Attributes:
        seq: nucleic acid sequence
        rev: reverse-complement of `seq`
        over_sense: 3' overhang
        over_antisense: 5' overhang

            - Positive value -> overhang at 3'
            - Negative value -> undercut at 3'
    Args:
        seq: A pre-encoded nucleotide array or Raw Sequence: allowed letters:
            - G, C, A, T(U)
            - S, R, M, K, Y, W
            - V, B, D, H
            - N
            - lower => masked
        over_sense: 3' overhang
        over_antisense: 5' overhang

            - Positive value -> overhang at 3'
            - Negative value -> undercut at 3'
        strict: fail upon encountering bad nucleotides (otherwise calls it 'n')
    """
    def __init__(self,
                 seq: Optional[Union[Sequence[str], np.ndarray]],
                 over_sense: int = 0,
                 over_antisense: int = 0,
                 strict: bool = False):
        self._strict = strict
        self._rev = None
        self._letters: Optional[str] = None
        self._seq = None
        if isinstance(seq, str):
            self._letters = seq
        elif isinstance(seq, np.ndarray):
            self._seq = seq
        elif seq is None:
            self._seq = np.array([], dtype=np.uint8)
        else:
            # assume sequence
            self._seq = encode(seq, strict=strict)
        # Following attributes have no use in RNAseqencing, are kept for future
        self.over_sense = over_sense
        self.over_antisense = over_antisense

    @property
    def seq(self) -> np.ndarray:
        if self._seq is None:
            if self._letters is None:  # pragma: no cover  # safety valve
                self._seq = np.array([], dtype=np.uint8)
            else:
                self._seq = encode(self._letters, strict=self._strict)
        return self._seq

    @seq.setter
    def seq(self, val: np.ndarray):  # pragma: no cover  # unused, for future
        self._seq = val
        self._rev = None  # reset
        self._letters = decode(val)

    @property
    def rev(self) -> np.ndarray:
        # Reverse complement strand sequence.
        # This is intentionally kept as @property, since
        # we don't want to call it unnecessarily.
        if self._rev is None:
            self._rev = revcomp(self.seq)
        return self._rev

    def __len__(self) -> int:
        return self.seq.size

    def __repr__(self):
        if len(self) > 130:
            return decode(self.seq[:100]) + "..."
        return decode(self.seq)

    def __getitem__(self, idx):
        return DNA(self.seq[idx])

    def __iter__(self):
        return self.seq.__iter__()

    def trim(self, start: int = 0, end: int = None):
        """
        Trim length of sequence to only include a sub-Sequence
        `Remember to trim FastQual sequence too`

        Args:
            start: index in current sequence at which new sequence starts
            end: index in current sequence at which new sequence ends
        """
        if end is None:
            end = len(self)  # if not provided, all of it
        end = min(end, len(self))  # clip to end
        self._seq = self.seq[start:end]
        self._rev = None  # reset for future calculation

    def to_str(self, u_case: bool = None, rev: bool = False) -> str:
        """
        String representation

        Args:
            case: ``None``: `as is`; ``True``: UPPER; ``False``: lower;
            rev: reverse complement
        """
        if u_case is None:
            return decode(self.rev) if rev else decode(self.seq)
        if u_case:
            return decode(self.rev & ~_MASK) if rev else decode(self.seq
                                                                & ~_MASK)
        return decode(self.rev | _MASK) if rev else decode(self.seq | _MASK)

    def get_strand(self, strand: int = 1) -> np.ndarray:
        """
        Get sequence of one strand (respecting overhangs)

        Args:
            strand: orientation of sequence (1 -> sense, -1 -> anti-sense)

        Returns:
            array representation of strand
        """
        if strand % 2:
            # sense strand
            return self.seq[max(0, -self.over_antisense
                                ):min(self.over_sense, len(self))]
        return self.rev[max(0, -self.over_sense
                            ):min(self.over_antisense, len(self))]

    def complement(self) -> 'DNA':
        """
        Create a DNA object that is reverse_complement

        Returns:
            Reverse-complement of the strand

        """
        return DNA(self.rev)

    def ligate(self, addendum: 'DNA', care_overhangs: bool = False) -> 'DNA':
        """
        Create a DNA molecule by ligating `addendum` at the 3' end

        Args:
            addendum: DNA object to be added
            care_overhangs: if overhangs don't match, raise error

        Returns:
            ligated product: `self` + `addendum`

        Raises:
            OverHangError: Overhangs don't match
        """
        if not care_overhangs:
            return DNA(np.concatenate(self.seq, addendum.seq))
        if self.over_sense + addendum.over_antisense != 0:
            raise OverHangError()
        # check if overhangs can pair up
        overhang = abs(self.over_sense)
        if self.seq[-overhang:] != addendum.seq[:overhang]:
            raise OverHangError()
        return DNA(np.concatenate(self.seq, addendum.seq))

    def cleave(self,
               position: int,
               anti_pos: int = None,
               throw: bool = True) -> Tuple['DNA', 'DNA']:
        """
        Cleave DNA at given position and return product DNAs

        Args:
            position: at which the second fragment starts (0-indexed)

                - (negative position: indexed from end)

            anti_pos: cleave anti-sequence at position [default: position]

                - (also indexed on the sense strand, starting 0)

            throw: throw error if position is beyond bounds?

                - if ``False``, returns `Tuple[self, DNA('')]`

        Returns:
            5' Fragment, 3' Fragment with overhangs, if any

        Raises:
            ValueError: `position` or `anti_pos` is greater than DNA's size
        """
        anti_pos = anti_pos if anti_pos is not None else position
        if (position > len(self) or anti_pos > len(self)) and throw:
            # position beyond DNA bounds
            raise ValueError(
                f"'{position} / {anti_pos}' > DNA's size: {len(self)}")
        position = min(position, len(self))
        anti_pos = min(anti_pos, len(self))
        position %= len(self)
        return (DNA(self.seq[:position], over_sense=position - anti_pos),
                DNA(self.seq[position:], over_antisense=anti_pos - position))
