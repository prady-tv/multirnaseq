*************************
multiRNAseq
*************************

Gist
==========

Source Code Repository
---------------------------

|source| `Repository <https://github.com/pradyumna/multiRNAseq.git>`__


Badges
---------

|Documentation Status|  |Coverage|  |PyPi Version|  |PyPi Format|  |PyPi Pyversion|


Description
==============

Create a python project from template.

What does it do
--------------------

Some great thing

.. |Documentation Status| image:: https://readthedocs.org/projects/multiRNAseq/badge/?version=latest
   :target: https://multiRNAseq.readthedocs.io/?badge=latest
.. |source| image:: https://github.githubassets.com/favicons/favicon.png
   :target: https://github.com/pradyumna/multiRNAseq.git

.. |PyPi Version| image:: https://img.shields.io/pypi/v/multiRNAseq
   :target: https://pypi.org/project/multiRNAseq/
   :alt: PyPI - version

.. |PyPi Format| image:: https://img.shields.io/pypi/format/multiRNAseq
   :target: https://pypi.org/project/multiRNAseq/
   :alt: PyPI - format

.. |PyPi Pyversion| image:: https://img.shields.io/pypi/pyversions/multiRNAseq
   :target: https://pypi.org/project/multiRNAseq/
   :alt: PyPi - pyversion

.. |Coverage| image:: docs/coverage.svg
   :alt: tests coverage
   :target: tests/htmlcov/index.html
