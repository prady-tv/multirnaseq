#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Parse HiSAT2 pair_gist files (--new-pair_gist) and plot
'''

import os
import re
from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional, Tuple

import pandas as pd
from matplotlib import pyplot as plt
from multiRNAseq.errors import FormatError
from scipy.stats import ttest_rel


@dataclass
class alignment():
    '''
    Alignment overview

    Attributes:
        total: total reads
        concord: concordant alignments
        discord: discordant alignments
        multihit: reads hitting multiple genome loci
        unalign: unaligned reads

    '''
    total: int
    concord: int
    discord: int
    multihit: int
    unalign: int


PAIRED_PAT = re.compile(
    r'\W+Total pairs: (\d+)\n\W+Aligned concordantly or discordantly 0 time: (\d+) \([\d\.]+%\)\n\W+Aligned concordantly 1 time: (\d+) \([\d\.]+%\)\n\W+Aligned concordantly >1 times: (\d+) \([\d\.]+%\)\n\W+Aligned discordantly 1 time: (\d+) \([\d\.]+%\)\n'
)
SINGLE_PAT = re.compile(
    r'\W+Total(?: unpaired)? reads: (\d+)\n\W+Aligned 0 time: (\d+) \([\d\.]+%\)\n\W+Aligned 1 time: (\d+) \([\d\.]+%\)\n\W+Aligned >1 times: (\d+) \([\d\.]+%\)'
)
OVERVW_PAT = re.compile(r'Overall alignment rate: ([\d\.]+)%')


def parse_new_sum(sum_p: Path) -> Tuple[bool, List[Optional[int]], float]:
    '''
    Parse summary file to fish out read statistics for:

        - Paired total
        - Paired unaligned
        - Paired unique
        - Paired multihit
        - Paired discordant
        - Single total
        - Single unaligned
        - Single unique
        - Single multihit
        - Single discordant
        - Overall alignment fraction

    Args:
        sum_p: path to summary file

    Returns:
        ?paired, list of stats, overall_align

    '''
    is_paired = True
    sum_vals: List[Optional[int]] = []
    with open(sum_p) as sum_h:
        sum_out = sum_h.read()
    paired = PAIRED_PAT.findall(sum_out)
    if len(paired) == 0:
        # No pattern found, may be a single end reads' summary
        paired = [None] * 5
        is_paired = False
    else:
        paired = [int(val) for val in paired[0]]
    sum_vals.extend(paired)
    singles = SINGLE_PAT.findall(sum_out)
    overvw = OVERVW_PAT.findall(sum_out)
    if len(singles) == 0 or len(overvw) == 0:
        raise FormatError(sum_p, singles, overvw)
    singles = [*(int(val) for val in singles[0]), None]
    sum_vals.extend(singles)
    return is_paired, sum_vals, float(overvw[0]) / 100


def compile_frames(root: os.PathLike):
    """
    Compile pandas dataframe from files in given dict
    """
    paired_df = pd.DataFrame(
        columns=['total', 'unaligned', 'exacthit', 'multihit', 'discordant'],
        index=pd.MultiIndex([[], [], []], [[], [], []],
                            names=['ref', 'alignment', 'lib_type']))
    single_df = pd.DataFrame(
        columns=['total', 'unaligned', 'exacthit', 'multihit', 'discordant'],
        index=pd.MultiIndex([[], []], [[], []], names=['ref', 'alignment']))
    for sumfile in Path(root).glob('SRR*_summary.txt'):
        align_idx = sumfile.name.replace('_summary.txt',
                                         '').replace('_BPK282A1',
                                                     '').replace('_HU3', '')
        reference = 'refseq' if 'BPK282A1' in sumfile.name else 'HU3'
        is_paired, vals, align_frac = parse_new_sum(sumfile)
        if is_paired:
            paired_df.loc[(reference, align_idx, 'paired'), :] = vals[:5]
            paired_df.loc[(reference, align_idx, 'single'), :] = vals[5:]
        else:
            single_df.loc[(reference, align_idx), :] = vals[5:]
    paired_df = paired_df.div(paired_df['total'], axis=0)
    single_df = single_df.div(single_df['total'], axis=0)
    paired_df.drop('total', axis=1, inplace=True)
    single_df.drop('total', axis=1, inplace=True)
    paired_df.drop('discordant', axis=1, inplace=True)
    single_df.drop('discordant', axis=1, inplace=True)

    grp = single_df['unaligned'].groupby(level=['ref'])
    print(grp.describe())
    means = grp.mean()
    yerr = grp.std()
    fig, ax = plt.subplots()
    means.plot.bar(ax=ax,
                   fig=fig,
                   yerr=yerr,
                   stacked=True,
                   title='exact hits read fraction')
    plt.xticks(rotation=0)
    # plt.savefig('unpaired_diff_between_references.jpg')
    plt.show()

    print(
        'paired ends',
        ttest_rel(paired_df.loc['HU3', 'exacthit'].sort_index(),
                  paired_df.loc['refseq', 'exacthit'].sort_index()))

    print(
        'single ends',
        ttest_rel(single_df.loc['HU3', 'exacthit'].sort_index(),
                  single_df.loc['refseq', 'exacthit'].sort_index()))


if __name__ == '__main__':
    compile_frames(Path(os.environ['HOME']) / 'temp')
