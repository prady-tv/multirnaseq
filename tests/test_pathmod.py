#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020, 2021 Pradyumna Paranjape
# This file is part of multiRNAseq.
# # multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Test pathmods
"""

from pathlib import Path
from unittest import TestCase

from multiRNAseq.pathmod import fastq_mod

DATA = Path(__file__).parent / 'data'


class TestMod(TestCase):
    stem = "test_reads"
    add = "add1", "add2", "add3"

    def test_fq(self):
        test_fq = Path(self.stem + ".fastq")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=False))
        self.assertEqual(test_mod, '_'.join([self.stem, *self.add]) + ".fastq")

    def test_fq2gz(self):
        test_fq = Path(self.stem + ".fastq")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=True))
        self.assertEqual(test_mod,
                         '_'.join([self.stem, *self.add]) + ".fastq.gz")

    def test_fq_gz(self):
        test_fq = Path(self.stem + ".fastq.gz")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=False))
        self.assertEqual(test_mod, '_'.join([self.stem, *self.add]) + ".fastq")

    def test_1_fq(self):
        test_fq = Path(self.stem + "_1.fastq")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=False))
        self.assertEqual(test_mod,
                         '_'.join([self.stem, *self.add]) + "_1.fastq")

    def test_2_fq(self):
        test_fq = Path(self.stem + "_2.fastq")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=False))
        self.assertEqual(test_mod,
                         '_'.join([self.stem, *self.add]) + "_2.fastq")

    def test_1_fq_gz(self):
        test_fq = Path(self.stem + "_1.fastq.gz")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=False))
        self.assertEqual(test_mod,
                         '_'.join([self.stem, *self.add]) + "_1.fastq")

    def test_1_2_fq_gz(self):
        test_fq = Path(self.stem + "_1_2.fastq.gz")
        test_mod = str(fastq_mod(*self.add, fpath=test_fq, gz=False))
        self.assertEqual(test_mod,
                         '_'.join([self.stem, *self.add]) + "_1_2.fastq")

    def test_fq_fastq(self):
        test_fq = Path(self.stem + '.fq.gz')
        test_mod = str(fastq_mod(fpath=test_fq, fq=False))
        self.assertEqual(test_mod, self.stem + '.fastq.gz')

    def test_fastq_fq(self):
        test_fq = Path(self.stem + '.fastq.gz')
        test_mod = str(fastq_mod(fpath=test_fq, fq=True))
        self.assertEqual(test_mod, self.stem + '.fq.gz')
