#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test module multiRNAseq.mark_exons.fastq_io
'''
from pathlib import Path
from unittest import TestCase

from multiRNAseq.biomol import DNA
from multiRNAseq.classes import TargetConfig
# from multiRNAseq.classes import TargetConfig
from multiRNAseq.mark_exons.fastq_io import FastQFile

DATA = Path(__file__).parent / 'data'

# class unTestFastQFile(TestCase):
#     config = TargetConfig()

#     def untest_init(self):
#         test_fqf = FastQFile(self.config,
#                              fname=Path('tests/data/test_reads_1.fastq'),
#                              rname=Path('tests/data/test_reads_2.fastq.gz'),
#                              offset=33)
#         self.assertEqual(test_fqf.fname, Path('tests/data/test_reads_1.fastq'))
#         self.assertEqual(test_fqf.rname,
#                          Path('tests/data/test_reads_2.fastq.gz'))
#         test_fqf.spawn_analysis()


class test_anal(TestCase):
    """
    Rest an analysis file
    """
    def setUp(self):
        config = TargetConfig()
        self.fq = FastQFile(
            config,
            DATA / 'real_fastq/test_1.fastq.gz',
            DATA / 'real_fastq/test_2.fastq.gz',
        )

    def un_test_analysis(self):
        self.fq.spawn_analysis(term=DNA('A' * 20), min_qual=14, max_reads=1000)
