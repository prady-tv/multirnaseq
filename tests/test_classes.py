#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test module multiRNAseq.mark_exons.knowledge
'''

import os
from pathlib import Path
from unittest import TestCase

from multiRNAseq.classes import Executables, Genome, Resources, TargetConfig
from multiRNAseq.errors import (DependencyError, DesignFileValueError,
                                TargetError)

DATA = Path(__file__).parent / 'data'


class test_Resources(TestCase):
    def setUp(self):
        self.test_res = {
            'root': '.',
            'genome': 'genomes',
            'rnaseq': 'rnaseq',
            'ncbi': 'ncbi'
        }
        self.resources = Resources(self.test_res)

    def tearDown(self):
        del self.test_res
        del self.resources

    def test_stddir(self):
        pwd = Path('.').resolve()
        self.resources.update(self.test_res)
        for stddir, default in self.test_res.items():
            setattr(self.resources, stddir, '.')
            self.assertEqual(str(getattr(self.resources, stddir)), '.')
            delattr(self.resources, stddir)
            self.assertEqual(
                getattr(self.resources, stddir).resolve(), pwd / default)


class test_TargetConfig(TestCase):
    def setUp(self):
        self.valdict = {'mismatch': '0.3'}
        self.config = TargetConfig(self.valdict)

    def tearDown(self):
        del self.config

    def test_update(self):
        self.config.update(self.valdict)

    def test_validate(self):
        self.assertRaises(TargetError, self.config.verify, {})
        self.config.update({'target': 'Escherichia coli'})
        self.config.verify(
            {'Escherichia coli': Genome('Escherichia coli', Path('.'))})
        self.assertRaises(TargetError, self.config.verify, {})
        self.config.update({'mismatch': '2'})
        self.assertRaises(
            DesignFileValueError, self.config.verify,
            {'Escherichia coli': Genome('Escherichia coli', Path('.'))})
        self.config.update({'informative length': '-2', 'mismatch': '0.1'})
        self.assertRaises(
            DesignFileValueError, self.config.verify,
            {'Escherichia coli': Genome('Escherichia coli', Path('.'))})


class test_executables(TestCase):
    def setUp(self):
        self.pathdict = {'fastq-dump': 'fastq-dump'}
        self.exec_env = Executables(self.pathdict)

    def tearDown(self):
        del self.exec_env

    def test_verified(self):
        dummy_file = DATA / 'bad_design.yml'
        if os.environ.get('GITLAB_CI_TESTING'):
            pathdict = {
                'execute': True,
                'aria2c': dummy_file,
                'prefetch': dummy_file,
                'fastq-dump': dummy_file,
                'hisat2': dummy_file,
                'hisat2-build': dummy_file,
                'gffread': dummy_file,
                'fastqc': dummy_file,
                'samtools': dummy_file,
                'trim_galore': dummy_file,
                'cutadapt': dummy_file,
                'TPMCalculator': dummy_file
            }
            self.exec_env.update(pathdict)
        self.assertIsNone(self.exec_env.verify())
