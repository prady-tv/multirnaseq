#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test module multiRNAseq.mark_exons.knowledge
'''

from unittest import TestCase

from multiRNAseq.biomol import DNA, BadNAError

DNA_SEQS = {
    'DNA_BON':
    ('ATGCRYKMSWBVDHNatgcrykmswbvdhn', 'ndhbvswkmrygcatNDHBVSWKMRYGCAT'),
    'DNA_WSP': ('ATGC RYKMSW  BVDH N\n atgc\trykmsw\tbvdh\tn',
                'n\tdhbv\tswkmry\tgcat \nN DHBV  SWKMRY GCAT'),
    'DNA_BAD':
    ('ATGCRYKMSWBVDHNZatgcrykmswbvdhnz', 'zndhbvswkmrygcatZNDHBVSWKMRYGCAT'),
}

RNA_SEQS = {
    'RNA_BON':
    ('AUGCRYKMSWBVDHNaugcrykmswbvdhn', 'ndhbvswkmrygcauNDHBVSWKMRYGCAU'),
    'RNA_WSP': ('AUGC RYKMSW  BVDH N\n augc\trykmsw\tbvdh\tn',
                'n\tdhbv\tswkmry\tgcau \nN DHBV  SWKMRY GCAU'),
    'RNA_BAD':
    ('AUGCRYKMSWBVDHNZaugcrykmswbvdhnz', 'zndhbvswkmrygcauZNDHBVSWKMRYGCAU'),
}


class TestRNA(TestCase):
    def test_rna(self):
        bon_rna = DNA(seq=RNA_SEQS['RNA_BON'][0], strict=True)
        wsp_rna = DNA(seq=RNA_SEQS['RNA_WSP'][0], strict=True)
        bon_rna = DNA(seq=list(RNA_SEQS['RNA_BON'][0]), strict=True)
        self.assertEqual(bon_rna.to_str(), RNA_SEQS['RNA_BON'][0])
        print(bon_rna.rev)
        self.assertEqual(bon_rna.to_str(rev=True), RNA_SEQS['RNA_BON'][1])
        self.assertEqual(bon_rna.to_str(u_case=True),
                         RNA_SEQS['RNA_BON'][0].upper())
        self.assertEqual(wsp_rna.to_str(), RNA_SEQS['RNA_BON'][0])
        self.assertEqual(wsp_rna.to_str(rev=True), RNA_SEQS['RNA_BON'][1])

    def test_bad(self):
        with self.assertRaises(BadNAError):
            bad_rna = DNA(seq=RNA_SEQS['RNA_BAD'][0], strict=True)
            print(bad_rna)  # raises error
        bad_rna = DNA(seq=RNA_SEQS['RNA_BAD'][0], strict=False)
        self.assertEqual(
            bad_rna.to_str(),
            RNA_SEQS['RNA_BAD'][0].replace('z', 'n').replace('Z', 'n'))
        self.assertEqual(
            bad_rna.to_str(rev=True),
            RNA_SEQS['RNA_BAD'][1].replace('z', 'n').replace('Z', 'n'))


class TestDNA(TestCase):
    def setUp(self):
        self.bon_dna = DNA(DNA_SEQS['DNA_BON'][0])
        self.bon_rna = DNA(RNA_SEQS['RNA_BON'][0])

    def test_dna(self):
        bon_dna = DNA(seq=DNA_SEQS['DNA_BON'][0], strict=True)
        wsp_dna = DNA(seq=DNA_SEQS['DNA_WSP'][0], strict=True)
        self.assertEqual(bon_dna.to_str(), DNA_SEQS['DNA_BON'][0])
        self.assertEqual(bon_dna.to_str(rev=True), DNA_SEQS['DNA_BON'][1])
        self.assertEqual(bon_dna.to_str(u_case=True),
                         DNA_SEQS['DNA_BON'][0].upper())
        self.assertEqual(wsp_dna.to_str(), DNA_SEQS['DNA_BON'][0])
        self.assertEqual(wsp_dna.to_str(rev=True), DNA_SEQS['DNA_BON'][1])

    def test_bad(self):
        with self.assertRaises(BadNAError):
            bad_dna = DNA(seq=DNA_SEQS['DNA_BAD'][0], strict=True)
            print(bad_dna)  # calculation made here
        bad_dna = DNA(seq=DNA_SEQS['DNA_BAD'][0], strict=False)
        self.assertEqual(
            bad_dna.to_str(),
            DNA_SEQS['DNA_BAD'][0].replace('z', 'n').replace('Z', 'n'))
        self.assertEqual(
            bad_dna.to_str(rev=True),
            DNA_SEQS['DNA_BAD'][1].replace('z', 'n').replace('Z', 'n'))

    def test_trim_none(self):
        self.bon_dna.trim()
        self.assertEqual(self.bon_dna.to_str(), DNA_SEQS['DNA_BON'][0])

    def test_trim_start(self):
        self.bon_dna.trim(start=5)
        self.assertEqual(self.bon_dna.to_str(), DNA_SEQS['DNA_BON'][0][5:])

    def test_trim_end(self):
        self.bon_dna.trim(end=5)
        self.assertEqual(self.bon_dna.to_str(), DNA_SEQS['DNA_BON'][0][:5])

    def test_trim_start_end(self):
        self.bon_dna.trim(start=5, end=10)
        self.assertEqual(self.bon_dna.to_str(), DNA_SEQS['DNA_BON'][0][5:10])

    def test_empty(self):
        self.assertEqual(DNA(seq=None).to_str(u_case=False), '')
        self.assertEqual(DNA('').complement().to_str(), '')
        self.assertEqual(DNA('').complement().to_str(), '')
