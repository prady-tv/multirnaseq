#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test readability of configuration
'''

import os
from pathlib import Path
from unittest import TestCase

from multiRNAseq.errors import (DependencyError, DesignFileFormatError,
                                DesignFileNotFoundError)
from multiRNAseq.read_config import read_config

DATA = Path(__file__).parent / 'data'


class test_ReadConfig(TestCase):
    """
    Test configuration
    """
    def setUp(self):
        self.default_config = read_config()

    def tearDown(self):
        del self.default_config

    def test_read_config(self):
        self.assertEqual(len(self.default_config.genomes), 0)
        self.assertEqual(len(self.default_config.phases), 0)

    def test_nodesign(self):
        self.assertRaises(DesignFileNotFoundError, read_config, "bad_config",
                          self.default_config)
        self.assertIsNotNone(
            read_config(DATA / 'design.yml', self.default_config))

    def test_validation(self):
        if os.environ.get('GITLAB_CI_TESTING'):
            return
        test_config = read_config(DATA / 'design.yml', self.default_config)
        self.assertRaises(DependencyError, test_config.validate)

    def test_bad_design(self):
        self.assertRaises(DesignFileFormatError, read_config,
                          DATA / 'bad_design.yml', self.default_config)
