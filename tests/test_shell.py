#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test readability of configuration
'''

from pathlib import Path
from subprocess import TimeoutExpired
from unittest import TestCase

from multiRNAseq.errors import DependencyError, ExternalError
from multiRNAseq.shell import Shell


class TestShell(TestCase):
    """
    test ability to call shell commands
    """
    def setUp(self):
        self.shell = Shell()
        self.shell.exec_env.execute = True

    def tearDown(self):
        del self.shell

    def test_dry(self):
        """
        test whether ls can be called
        """
        # dry
        self.shell.exec_env.execute = False
        self.assertEqual(self.shell.process_comm('ls'), "DRY RUN")

    def test_execute(self):
        self.assertIsNotNone(self.shell.process_comm('ls'))
        self.assertIsNotNone(self.shell.process_comm('ls'))

    def test_timeout(self):
        self.assertRaises(TimeoutExpired,
                          self.shell.process_comm,
                          'sleep',
                          10,
                          timeout=1)

    def test_disowned(self):
        self.assertIsNone(self.shell.process_comm('ls', timeout=-1))

    def test_bad_cmd(self):
        self.assertIsNone(self.shell.process_comm('ls', 'badf'))
        self.assertIsNone(self.shell.process_comm('ls', 'badf', fail='notify'))
        self.assertRaises(ExternalError,
                          self.shell.process_comm,
                          'ls',
                          'badf',
                          fail='raise')
        self.assertRaises(FileNotFoundError, self.shell.process_comm,
                          'bad_cmd')

    def test_exec_env(self):
        self.shell.exec_env.dependencies['ls'] = Path('/bin/ls')
        self.assertRaises(DependencyError, self.shell.process_comm, 'ls')
        self.shell.exec_env._verified = True
        self.assertIsNotNone(self.shell.process_comm('ls'))
