#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test module multiRNAseq.mark_exons.sequencing
'''

from pathlib import Path
from unittest import TestCase

from multiRNAseq.classes import TargetConfig
from multiRNAseq.errors import BadFastqRecordError
from multiRNAseq.mark_exons.fastq_io import FastQFile
from multiRNAseq.mark_exons.sequencing import FastQual


class TestFastQual(TestCase):
    def setUp(self):
        self.qual = tuple(range(0, 94))
        self.bon33 = ''.join(tuple(chr(nuc_q + 33) for nuc_q in self.qual))
        self.bon64 = ''.join(tuple(chr(nuc_q + 64) for nuc_q in self.qual))
        self.bon33_qual = FastQual(self.bon33)
        self.bon33_qual.guess_type()
        self.bon64_qual = FastQual(self.bon64)

    def test_64(self):
        self.assertEqual(64, FastQual(offset=64).\
                         guess_type([self.bon64]))
        self.assertEqual(64, FastQual(self.bon64[0]).guess_type())

    def test_33(self):
        self.assertEqual(33, FastQual(offset=33).\
                         guess_type([self.bon33]))
        self.assertEqual(33, FastQual(self.bon33[0]).guess_type())

    def test_trim_none(self):
        self.bon33_qual.trim()
        self.assertEqual(self.bon33_qual.qual.tolist(), list(self.qual))

    def test_trim_start(self):
        self.bon33_qual.trim(start=5)
        self.assertEqual(self.bon33_qual.qual.tolist(), list(self.qual[5:]))

    def test_trim_end(self):
        self.bon33_qual.trim(end=10)
        self.assertEqual(self.bon33_qual.qual.tolist(), list(self.qual[:10]))

    def test_trim_start_end(self):
        self.bon33_qual.trim(start=5, end=10)
        self.assertEqual(self.bon33_qual.qual.tolist(), list(self.qual[5:10]))


class UnTestFastqFile(TestCase):
    config = TargetConfig()

    def untest_single(self):
        single_end = FastQFile(self.config,
                               Path("tests/data/test_reads_1.fastq"))
        self.assertFalse(single_end.read_h[0][0].closed)

    def untest_mate(self):
        mates = FastQFile(self.config, Path("tests/data/test_reads_1.fastq"),
                          Path("tests/data/test_reads_2.fastq.gz"))
        self.assertFalse(mates.read_h[0][0].closed)
        self.assertFalse(mates.read_h[0][0].closed)

    def untest_bad_fastq(self):
        self.assertRaises(
            BadFastqRecordError, lambda: FastQFile(
                self.config, Path("tests/data/empty_file.fastq")))
