#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test module multiRNAseq.mark_exons.subject
'''

from unittest import TestCase

from multiRNAseq.biomol import DNA
from multiRNAseq.mark_exons.subject import subject


class TestSubject(TestCase):
    '''
    Test subject object for integrity
    '''
    def test_default(self):
        test_sub_default = subject()
        self.assertEqual(test_sub_default.inform, 14)
        self.assertEqual(test_sub_default.lead, None)
        self.assertEqual(test_sub_default.term, None)
        self.assertEqual(test_sub_default.min_qual, 30)
        self.assertEqual(test_sub_default.read_min, 20)
        self.assertEqual(test_sub_default.mismatch, 0.01)

    def test_alter(self):
        test_sub_alter = subject(inform=15,
                                 lead=DNA('ttct'),
                                 term=DNA('AAAA'),
                                 min_qual=31,
                                 read_min=21,
                                 mismatch=0.02,
                                 garbage='garbage')
        self.assertEqual(test_sub_alter.inform, 15)
        self.assertEqual(test_sub_alter.min_qual, 31)
        self.assertEqual(test_sub_alter.read_min, 21)
        self.assertEqual(test_sub_alter.mismatch, 0.02)
        self.assertEqual(test_sub_alter.lead.to_str(), 'ttct')
        self.assertEqual(test_sub_alter.term.to_str(), 'AAAA')
        self.assertRaises(AttributeError,
                          lambda: getattr(test_sub_alter, 'garbage'))
