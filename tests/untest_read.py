#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2020 Pradyumna Paranjape
# This file is part of multiRNAseq.
#
# multiRNAseq is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# multiRNAseq is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with multiRNAseq.  If not, see <https://www.gnu.org/licenses/>.
#
'''
Test module multiRNAseq.mark_exons.knowledge
'''

from random import randint
from unittest import TestCase

from multiRNAseq.biomol import DNA
from multiRNAseq.mark_exons.sequencing import Read
from multiRNAseq.native_parallel import find_edge

OFFSET = 33
READ_LEN = 40


class TestRead(TestCase):
    def setUp(self):
        self.bon_qual = tuple((randint(37, 40)) for _ in range(READ_LEN))
        self.bon_record = (
            '@bon read',
            ''.join(['A', 'T', 'G', 'C'][randint(0, 3)]
                    for _ in range(READ_LEN)),
            ''.join(chr(qual + OFFSET) for qual in self.bon_qual),
        )
        self.bon_read = Read(self.bon_record, offset=OFFSET)
        self.bon_seq = DNA(self.bon_record[1])

    def test_init(self):
        '''
        test reads
        '''
        self.assertEqual(self.bon_read.offset, OFFSET)
        self.assertEqual(self.bon_read.seq_id, self.bon_record[0])
        self.assertEqual(self.bon_read.qual.off33, self.bon_record[-1])
        self.assertEqual(len(self.bon_read), len(self.bon_seq))
        self.assertEqual(self.bon_read[:5].to_str(), self.bon_record[1][:5])
        self.assertEqual(self.bon_read.seq.to_str(), self.bon_seq.to_str())
        self.assertEqual(self.bon_read.qual.off33, self.bon_record[2])

    def test_mate(self):
        '''
        test name of mate
        '''
        self.assertEqual(self.bon_read.mate_id, self.bon_read.seq_id)
        self.bon_read.seq_id = r'@bon read\2'
        self.assertEqual(self.bon_read.guess_mate_id(), r'@bon read\1')
        self.bon_read.seq_id = r'@bon read\1'
        self.assertEqual(self.bon_read.guess_mate_id(), r'@bon read\2')


class TestTrim(TestCase):
    def setUp(self):
        self.seq_qual = tuple(range(READ_LEN))
        self.seq_record = (
            '@seq read',
            ''.join((nuc * (READ_LEN // 4) for nuc in ['A', 'T', 'G', 'C'])),
            ''.join(chr(qual + OFFSET) for qual in self.seq_qual),
        )
        self.seq_read = Read(self.seq_record, offset=OFFSET)
        self.seq_seq = DNA(self.seq_record[1])

    def test_trim_none(self):
        self.seq_read.trim()
        self.assertEqual(self.seq_read.qual.qual.tolist(), list(self.seq_qual))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1])

    def test_trim_start(self):
        self.seq_read.trim(start=5)
        self.assertEqual(self.seq_read.qual.qual.tolist(),
                         list(self.seq_qual[5:]))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1][5:])

    def test_trim_end(self):
        self.seq_read.trim(end=10)
        self.assertEqual(self.seq_read.qual.qual.tolist(),
                         list(self.seq_qual[:10]))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1][:10])

    def test_trim_start_end(self):
        self.seq_read.trim(start=5, end=10)
        self.assertEqual(self.seq_read.qual.qual.tolist(),
                         list(self.seq_qual[5:10]))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1][5:10])

    def test_rev_trim_none(self):
        self.seq_read.trim(reverse=True)
        self.assertEqual(self.seq_read.qual.qual.tolist(), list(self.seq_qual))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1])

    def test_rev_trim_start(self):
        self.seq_read.trim(start=5, reverse=True)
        self.assertEqual(self.seq_read.qual.qual.tolist(),
                         list(self.seq_qual[:-5]))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1][:-5])

    def test_rev_trim_end(self):
        self.seq_read.trim(end=10, reverse=True)
        self.assertEqual(self.seq_read.qual.qual.tolist(),
                         list(self.seq_qual[-10:]))
        self.assertEqual(self.seq_read.seq.to_str(), self.seq_record[1][-10:])

    def test_rev_trim_start_end(self):
        self.seq_read.trim(start=5, end=10, reverse=True)
        self.assertEqual(self.seq_read.qual.qual.tolist(),
                         list(self.seq_qual[-10:-5]))
        self.assertEqual(self.seq_read.seq.to_str(),
                         self.seq_record[1][-10:-5])


class TestBad(TestCase):
    def test_bad(self):
        bad_record = ('@bad read', 'N' * READ_LEN,
                      ''.join([chr(27 + OFFSET)] * READ_LEN))

        bad_read = Read(bad_record, OFFSET)
        bad_read.trim_bad(20)
        self.assertEqual(bad_read.seq.to_str(), bad_record[1])
        bad_read.trim_bad()
        self.assertEqual(bad_read.seq.to_str(), '')

    def test_tip(self):
        tip_record = ('@tip read', 'N' * READ_LEN,
                      ''.join([chr(15 + OFFSET)] * 6 + [chr(40 + OFFSET)] *
                              (READ_LEN - 10) + [chr(15 + OFFSET)] * 4))
        tip_read = Read(tip_record, OFFSET)
        tip_read.trim_bad()
        self.assertEqual(tip_read.seq.to_str(), tip_record[1][6:-4])

    def test_gap(self):
        gap_record = ('@gap read', 'N' * READ_LEN,
                      ''.join([chr(15 + OFFSET)] * 5 +
                              [chr(40 + OFFSET)] * 14 +
                              [chr(15 + OFFSET)] * 5 + [chr(40 + OFFSET)] *
                              (READ_LEN - 29) + [chr(15 + OFFSET)] * 5))
        gap_read = Read(gap_record, OFFSET)
        gap_read.trim_bad()
        self.assertEqual(gap_read.seq.to_str(), gap_record[1][5:19])


class TestLead(TestCase):
    def setUp(self):
        self.leader = '''
        AGGAATTCCGTACTGATCGGGCAGCAATGCATGAGTCTCAGAGCATCTAAACC
        CGGGGAGGCAGCTTAGGCCCGTATTGTTACGCCCGAGCAGGCCTATCAAGTTC
        TGGAAGTGATCGAAAGCAGGGTCTATTGCTGGGTCGATGGCTGCTATCAACAC
        '''.replace("\n", '').replace(" ", '')
        self.term = 'A' * 200

    def test_empty(self):
        mty_record = ('@mty read', 'N' * READ_LEN,
                      ''.join([chr(40 + OFFSET)] * READ_LEN))
        mty_read = Read(mty_record, OFFSET)
        edge, start, end = find_edge(mty_read.seq.seq,
                                     lead=DNA('').seq,
                                     rev_lead=DNA('').seq,
                                     term=DNA('').seq,
                                     rev_term=DNA('').seq)
        self.assertEqual(edge, 0)
        mty_read.trim(start, end)
        # nothing trimmed
        self.assertEqual(mty_read.seq.to_str(), mty_record[1])

    def test_none(self):
        none_record = ('@none read', '-' * READ_LEN,
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        none_read = Read(none_record, OFFSET)
        edge, start, end = find_edge(none_read.seq.seq,
                                     lead=DNA(self.leader).seq,
                                     rev_lead=DNA(self.leader).rev,
                                     term=DNA(self.term).seq,
                                     rev_term=DNA(self.term).rev)
        self.assertEqual(edge, 0)
        none_read.trim(start, end)
        # nothing trimmed
        self.assertEqual(none_read.seq.to_str(), none_record[1])

    def test_lead(self):
        lead_record = ('@lead read', self.leader[-20:] + '-' * (READ_LEN - 20),
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        lead_read = Read(lead_record, OFFSET)
        edge, start, end = find_edge(lead_read.seq.seq,
                                     lead=DNA(self.leader).seq,
                                     rev_lead=DNA(self.leader).rev,
                                     term=DNA(self.term).seq,
                                     rev_term=DNA(self.term).rev,
                                     inform=14)
        self.assertEqual(edge, 1)
        lead_read.trim(start, end)
        self.assertEqual(lead_read.seq.to_str(), lead_record[1][20:])
        revc_record = ('@revc read', DNA(lead_record[1]).to_str(rev=True),
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        revc_read = Read(revc_record, OFFSET)
        edge, start, end = find_edge(revc_read.seq.seq,
                                     lead=DNA(self.leader).seq,
                                     rev_lead=DNA(self.leader).rev,
                                     term=DNA(self.term).seq,
                                     rev_term=DNA(self.term).rev,
                                     inform=14)
        self.assertEqual(edge, 1)
        revc_read.trim(start, end)
        self.assertEqual(revc_read.seq.to_str(), revc_record[1][:20])
        mism_record = ('@mism read',
                       "---" + self.leader[-17:] + '-' * (READ_LEN - 20),
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        mism_read = Read(mism_record, OFFSET)
        edge, _, _ = find_edge(mism_read.seq.seq,
                               lead=DNA(self.leader).seq,
                               rev_lead=DNA(self.leader).rev,
                               term=DNA(self.term).seq,
                               rev_term=DNA(self.term).rev)
        self.assertEqual(edge, 0)

    def test_term(self):
        term_record = ('@term read', '-' * (READ_LEN - 20) + self.term[:20],
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        term_read = Read(term_record, OFFSET)
        edge, start, end = find_edge(read=term_read.seq.seq,
                                     lead=DNA(self.leader).seq,
                                     rev_lead=DNA(self.leader).rev,
                                     term=DNA(self.term).seq,
                                     rev_term=DNA(self.term).rev,
                                     inform=14)
        self.assertEqual(edge, -1)
        term_read.trim(start, end)
        self.assertEqual(term_read.seq.to_str(), term_record[1][:20])
        revc_record = ('@revc read', DNA(term_record[1]).to_str(rev=True),
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        revc_read = Read(revc_record, OFFSET)
        edge, start, end = find_edge(read=revc_read.seq.seq,
                                     lead=DNA(self.leader).seq,
                                     rev_lead=DNA(self.leader).rev,
                                     term=DNA(self.term).seq,
                                     rev_term=DNA(self.term).rev,
                                     inform=14)
        self.assertEqual(edge, -1)
        revc_read.trim(start, end)
        self.assertEqual(revc_read.seq.to_str(), revc_record[1][20:])
        mism_record = ('@mism read',
                       '-' * (READ_LEN - 20) + self.term[:17] + "---",
                       ''.join([chr(40 + OFFSET)] * READ_LEN))
        mism_read = Read(mism_record, OFFSET)
        edge, _, _ = find_edge(read=mism_read.seq.seq,
                               lead=DNA(self.leader).seq,
                               rev_lead=DNA(self.leader).rev,
                               term=DNA(self.term).seq,
                               rev_term=DNA(self.term).rev)
        self.assertEqual(edge, 0)
